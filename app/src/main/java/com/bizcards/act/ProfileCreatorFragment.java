package com.bizcards.act;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bizcards.act.interfaceAll.GetGroupId;
import com.bizcards.act.pojo.GroupCards;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.ContentValues.TAG;
//import static com.bizcards.act.MainActivity.card_view_Home;
import static com.bizcards.act.MainActivity.FullImagePath;
import static com.bizcards.act.MainActivity.ImageCaptureBitmap;
import static com.bizcards.act.MainActivity.linearFooter;
//import static com.bizcards.act.MainActivity.linearProfile_details;
import static com.bizcards.act.MainActivity.masterFragmentManager1;

public class ProfileCreatorFragment extends Fragment {


    Context mContext;
    Button btn_addnew_group;
    RelativeLayout rel_lable;
    RecyclerView list_of_new_group;
    GroupAdapter grpadpter;
    LinearLayoutManager mLayoutManager;
    Button imgbtn_plus;
    Boolean boolean_group_hideshow = false;

    private Profile profile;

    private EditText nameInput,input_address,input_website;

    private EditText jobTitleInput;
    private EditText companyInput;
    private EditText telephoneInput;
    private EditText emailInput;

    private Button nameCandidatesButton,website_candidates_button,address_candidates_button;
    private Button jobTitleCandidatesButton;
    private Button companyCandidatesButton;
    private Button telephoneCandidatesButton;
    private Button emailCandidatesButton;

    Map<String, Integer> phoneNumberCandidates = new HashMap<String, Integer>();
    Map<String, Integer> emailCandidates = new HashMap<String, Integer>();
    List<String> genericCandidates = new ArrayList<String>();
    List<String> nameCandidates = new ArrayList<String>();
    List<String> companyCandidates = new ArrayList<String>();

    List<String> WebsiteArrayList = new ArrayList<String>();
    List<String> AddressArrayList = new ArrayList<String>();

    private ProfileDao profileDao;
    String strSetting;;

    ImageView imgback,imgset;


    ArrayList<String>  selectedGroupId;

    byte[]  FullImageData = null;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.activity_profile_creator, container, false);
        mContext = ProfileCreatorFragment.this.getActivity();


        imgset = (ImageView)  root.findViewById(R.id.imgset);
        //input_address,input_website
        input_website =  (EditText) root.findViewById(R.id.input_website);
        input_address =  (EditText) root.findViewById(R.id.input_address);
        nameInput = (EditText) root.findViewById(R.id.input_name);
        jobTitleInput = (EditText) root.findViewById(R.id.input_job_title);
        companyInput = (EditText) root.findViewById(R.id.input_company);
        telephoneInput = (EditText) root.findViewById(R.id.input_telephone);
        emailInput = (EditText) root.findViewById(R.id.input_email);
        nameCandidatesButton = (Button) root.findViewById(R.id.name_candidates_button);
        imgback = (ImageView)  root.findViewById(R.id.imgback);
        address_candidates_button = (Button) root.findViewById(R.id.address_candidates_button);
        website_candidates_button = (Button) root.findViewById(R.id.website_candidates_button);
        jobTitleCandidatesButton = (Button) root.findViewById(R.id.job_title_candidates_button);
        companyCandidatesButton = (Button) root.findViewById(R.id.company_candidates_button);
        telephoneCandidatesButton = (Button) root.findViewById(R.id.telephone_candidates_button);
        emailCandidatesButton = (Button) root.findViewById(R.id.email_candidates_button);
        imgbtn_plus = (Button)root.findViewById(R.id.imgbtn_plus);
        btn_addnew_group = (Button)root.findViewById(R.id.btn_addnew_group);
        rel_lable = (RelativeLayout)root.findViewById(R.id.rel_lable);
        list_of_new_group = (RecyclerView) root.findViewById(R.id.list_of_new_group);

        btn_addnew_group.setVisibility(View.GONE);
        list_of_new_group.setVisibility(View.GONE);




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    masterFragmentManager1.popBackStack();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btn_addnew_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CreateGroup();
            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ///imgMenu.setImageResource(R.drawable.menu);
//                linear1.setVisibility(View.VISIBLE);
                //card_view_Home.setVisibility(View.VISIBLE);
                linearFooter.setVisibility(View.VISIBLE);
                //linearProfile_details.setVisibility(View.GONE);

                try {
                    masterFragmentManager1.popBackStack();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



        AsyncTaskProfileview asyncTaskProfile =new AsyncTaskProfileview();
        asyncTaskProfile.execute("");

      /*  ((AppCompatActivity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!generateProfile()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(R.string.profile_creator_alert_read_fail);
                    builder.setNegativeButton(R.string.profile_creator_alert_read_fail_manual,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    builder.show();

                }

            }
        });*/




        imgbtn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!boolean_group_hideshow){

                    boolean_group_hideshow = true;
                   // btn_addnew_group.setVisibility(View.VISIBLE);
                    //list_of_new_group.setVisibility(View.VISIBLE);

                    if(strSetting.equalsIgnoreCase("true")){
                      //  switchOnOff.setChecked(true);

                        btn_addnew_group.setVisibility(View.VISIBLE);
                        list_of_new_group.setVisibility(View.VISIBLE);
                    }else {
                        //switchOnOff.setChecked(false);
                        Toast.makeText(mContext,"Please Enable Group on From Setting !",Toast.LENGTH_LONG).show();
                    }

                }else{

                    boolean_group_hideshow = false;
                    btn_addnew_group.setVisibility(View.GONE);
                    list_of_new_group.setVisibility(View.GONE);
                }

            }
        });


        profileDao = ProfileDao.getInstance(mContext);



        Button saveButton = (Button) root. findViewById(R.id.save_button);
        Button rescanButton = (Button) root. findViewById(R.id.rescan_button);
        Button exitButton = (Button) root.findViewById(R.id.exit_button);

        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){


                String tempstrcurrent_Date="validation checkonly";

                Profile profile = new Profile("mid",
                        nameInput.getText().toString(),
                        jobTitleInput.getText().toString(),
                        companyInput.getText().toString(),
                        telephoneInput.getText().toString(),
                        emailInput.getText().toString(),input_website.getText().toString(),input_address.getText().toString(),tempstrcurrent_Date,"false",groupId,
                        FullImageData,FullImagePath );

                //Here first we do check validation then will do save process
                if (profile.isValid()){

                    AsyncTaskSave asyncTask=new AsyncTaskSave();
                    asyncTask.execute("");

                }else {
                    alertInvalidProfile();
                }


            }
        });
        rescanButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                confirmRescan();
            }
        });
        exitButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                confirmExit();
            }
        });



        address_candidates_button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(AddressArrayList, input_address);
            }
        });

        website_candidates_button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(WebsiteArrayList, input_website);
            }
        });

        nameCandidatesButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(nameCandidates, nameInput);
            }
        });


        jobTitleCandidatesButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(genericCandidates, jobTitleInput);
            }
        });
        companyCandidatesButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(companyCandidates, companyInput);
            }
        });


        telephoneCandidatesButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(phoneNumberCandidates.keySet(), telephoneInput);
            }
        });
        emailCandidatesButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                popUpCandidates(emailCandidates.keySet(), emailInput);
            }
        });



        /*((AppCompatActivity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setUp();
            }
        });*/

        //setUp();
        //getGroupList();

        return root;
        }


//    ArrayList<String> grpdata;

    ArrayList<String> grouNameMatching;

    private void setUp() {



        try {
            getGroupList();
        } catch (Exception e) {
            e.printStackTrace();
        }

      /*  grpdata = new ArrayList<>();
        grpdata.add("Business");
        grpdata.add("Family");
        grpdata.add("Vip");
        grpdata.add("Colleagues");
        grpdata.add("Customer");*/


        if (!grouNameMatching.contains("Business")) {

            try {

                ArrayList<Profile>  BProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Business", BProfiles);
                saveGroup(groupCard);
                //validateAndCreateGroup("Business");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!grouNameMatching.contains("Family")) {

            try {

                ArrayList<Profile>  FProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Family", FProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Family");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Vip")) {

            try {

                ArrayList<Profile>  VProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Vip", VProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Vip");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Colleagues")) {

            try {

                ArrayList<Profile>  CProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Colleagues", CProfiles);
                saveGroup(groupCard);
                //validateAndCreateGroup("Colleagues");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Customer")) {

            try {

                ArrayList<Profile>  CUSProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Customer", CUSProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Customer");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {
            getGroupList();
        } catch (Exception e) {
            e.printStackTrace();
        }



       // prepareDemoContent();

    }


    private boolean generateProfile() {

        ArrayList<String> profileData;
        try{

            Bundle bundle = this.getArguments();
            profileData = bundle.getStringArrayList(CameraReaderActivity.PROFILE_DATA_KEY);
        } catch(Exception e) {
            Log.w(ProfileCreatorActivity.class.getName(), Log.getStackTraceString(e));
            return false;
        }

             // input_address,
               // input_website

//        String mm ="abcdefgwww.google.com ttt";
//

        //imgset

        try{
            imgset.setImageBitmap(ImageCaptureBitmap);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            ImageCaptureBitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
//            FullImageData = stream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            for (String snapshot : profileData){

                try {
                    for (String text : snapshot.split("\n")){

                        try{

                            String strWeb =  getWeb(text);
                            input_website.setText(strWeb);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try{
                            String strAddress =  getAddress(text);
                            input_address.setText(strAddress);
                        }catch (Exception e){
                            e.printStackTrace();
                        }



                        int selected = 0;
                        try {
                            selected = selectPhoneNumber(text, phoneNumberCandidates)
                                    + selectEmail(text, emailCandidates);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            if (selected == 0) {
                                selectRest(text, genericCandidates);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        boolean generateProfile = false;
        try {
            generateProfile = false;
            String phoneNumber = getBestCandidate(phoneNumberCandidates);
            if (StringUtils.isNotBlank(phoneNumber)){
                generateProfile = true;
                telephoneInput.setText(phoneNumber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        String email = null;
        try {
            email = getBestCandidate(emailCandidates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (StringUtils.isNotBlank(email)){
            generateProfile = true;
            emailInput.setText(email);
            String namePart = email.substring(0, email.indexOf("@"));
            String companyPart = email.substring(email.indexOf("@")+1, email.length());
            companyPart = companyPart.substring(0, companyPart.indexOf("."));

            StringBuilder nameBuilder = new StringBuilder();

            int j = 0;
            try {
                for (String str : namePart.split("\\.")){
                    j++;
                    nameBuilder.append(str.substring(0, 1).toUpperCase());
                    if (str.length() > 1){
                        nameBuilder.append(str.substring(1));
                    }
                    nameBuilder.append(" ");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (j > 0) {
                nameCandidates.add(nameBuilder.toString().trim());
            }

            if (companyPart.length() > 1
                    && !companyPart.equals("googlemail")
                    && !companyPart.equals("gmail")
                    && !companyPart.equals("hotmail")
                    && !companyPart.equals("live")){
                companyCandidates.add(companyPart.substring(0, 1).toUpperCase()+companyPart.substring(1));
                companyCandidates.add(companyPart.toUpperCase());
                companyCandidates.add(companyPart);
            }

        }


        nameCandidates.addAll(genericCandidates);

        companyCandidates.addAll(genericCandidates);

        if (!nameCandidates.isEmpty()){
            nameInput.setText(nameCandidates.get(0));
            generateProfile = true;
        }

        int i = 0;

        if (!companyCandidates.isEmpty()){
            if (companyCandidates.get(0).equals(nameCandidates.get(0)) && companyCandidates.size() != 1){
                i++;
            }
            companyInput.setText(companyCandidates.get(i));
        }

        if (!genericCandidates.isEmpty()){
            jobTitleInput.setText(genericCandidates.get(0));
        }

        genericCandidates.addAll(phoneNumberCandidates.keySet());
        genericCandidates.addAll(emailCandidates.keySet());


        return generateProfile;

    }

    private void selectRest(String text, List<String> genericCandidates) {
        List<String> toFilter = new ArrayList<String>();
        boolean filter = false;
        for (String candidate : genericCandidates){
            if (candidate.contains(text)){
                filter = true;
                break;
            }
            if (text.contains(candidate)){
                toFilter.add(candidate);
            }
        }
        if (!filter){
            genericCandidates.add(text);
        }
        genericCandidates.removeAll(toFilter);
    }


    private String  getWeb (String text){

        String strweb = "";

         if(text.contains("www")){


             try{

                 String[] allWeb = text.split(" ");
                 for(int m=0;m<allWeb.length;m++){
                     String strWeb = allWeb[m];
                     WebsiteArrayList.add(strWeb);
                 }
             }catch (Exception e){
                 e.printStackTrace();
             }


             String[] webtext = text.split("www.");
             String[] getWeb = webtext[1].split(" ");
             strweb =  "www."+getWeb[0];

         }else{
             strweb= "";
         }

        return  strweb;
    }


    private String  getAddress (String text){

        String straddress = "";

        if(text.contains(",")|| text.contains("address") || text.contains("Nr.") || text.contains("Road") || text.contains("street") ){

            try{

                String[] allWeb = text.split(",");
                for(int m=0;m<allWeb.length;m++){
                    String stradd = allWeb[m];
                    AddressArrayList.add(stradd);
                }
            }catch (Exception e){
                e.printStackTrace();
            }


            String[] webtext = text.split("www.");
            String[] getWeb = webtext[1].split(" ");
            straddress =  "www."+getWeb[0];

        }else{
            straddress= "";
        }

        return  straddress;
    }



    private int selectPhoneNumber(String text, Map<String, Integer> phoneNumberCandidates) {
        //At least 6 numbers, allow other characters
        String trimmed = text.toLowerCase().replaceAll("tel:","").replaceAll("mob:","").trim();
        if (phoneNumberCandidates.containsKey(trimmed)) {
            phoneNumberCandidates.put(trimmed, phoneNumberCandidates.get(trimmed) + 1);
        } else {

            int numCount = 0;
            for (char c : trimmed.toCharArray()) {
                if (Character.isDigit(c)) {
                    numCount++;
                }
                if (numCount == 6) {
                    phoneNumberCandidates.put(trimmed, 1);
                    return 1;
                }
            }
        }
        return 0;
    }


    private int selectEmail(String text, Map<String, Integer> emailCandidates) {
        int atPos = text.indexOf("@");
        int dotPos = text.lastIndexOf(".");
        //Very basic check to see if a text COULD BE an email address
        if (atPos != -1 && dotPos > atPos){
            String trimmed = text.trim();
            if (emailCandidates.containsKey(trimmed)){
                emailCandidates.put(trimmed, emailCandidates.get(trimmed)+1);
            } else {
                emailCandidates.put(trimmed, 1);
            }
            return 1;
        }
        return 0;
    }

    private String getBestCandidate(Map<String, Integer> candidates){
        int maxValue = 0;
        String bestCandidate ="";
        for (Map.Entry<String, Integer> candidate : candidates.entrySet()){
            if (candidate.getValue() > maxValue){
                maxValue = candidate.getValue();
                bestCandidate = candidate.getKey();
            }
        }
        //candidates.remove(bestCandidate);
        return bestCandidate;
    }

  /*  @Override
    public void onBackPressed(){
//        Intent intent = new Intent(ProfileCreatorActivity.this, MainActivity.class);
//        startActivity(intent);
//        finish();
    }*/


  String groupId = "";

    private void validateAndCreateProfile(){


        for(int m=0;m<selectedGroupId.size();m++){
            groupId = selectedGroupId.get(m);
        }

//        Toast.makeText(mContext,"GId--"+groupId,Toast.LENGTH_LONG).show();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        String strcurrent_Date = sdf.format(new Date());
//        Log.d(TAG, "Date: " + str);
//        String strcurrent_Date = "2020/06/22";
        //input_address,input_website


//        byte[] getimage = null;


        Profile profile = new Profile("mid",
                nameInput.getText().toString(),
                jobTitleInput.getText().toString(),
                companyInput.getText().toString(),
                telephoneInput.getText().toString(),
                emailInput.getText().toString(),input_website.getText().toString(),input_address.getText().toString(),strcurrent_Date,"false",groupId,
                FullImageData,FullImagePath );


        if (profile.isValid()){

            if (saveProfile(profile)) {

                showSaveSuccessDialog();

            } else {

                Utils.displayErrorDialog(mContext);
            }
        } else {
            alertInvalidProfile();
        }
    }

    private boolean saveProfile(Profile profile) {
        return profileDao.insert(profile);
    }

    private void alertInvalidProfile() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.profile_creator_save_invalid_title);
        builder.setMessage(R.string.profile_creator_save_invalid_message);
        builder.setPositiveButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        builder.create().show();
    }

    private void showSaveSuccessDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.dialog_success);
        builder.setMessage(R.string.profile_creator_save_success_message);
        builder.setCancelable(false); //Don't let them touch out!
        builder.setNegativeButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

//                        card_view_Home.setVisibility(View.VISIBLE);
                        linearFooter.setVisibility(View.VISIBLE);


                        try {
                            dialogInterface.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        MasterHomeFrgament ProfileListAct = new MasterHomeFrgament();
                        Utils.SetFragment(ProfileListAct,mContext , "HomeFragments");

                    }
                });
       /* builder.setPositiveButton(R.string.profile_creator_save_success_scan_another,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      *//*  Intent intent = new Intent(ProfileCreatorActivity.this,
                                CameraReaderActivity.class);
                        startActivity(intent);
                        finish();*//*
                    }
                });
        */

        builder.create().show();
    }


    private void showGroupSaveSuccessDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.dialog_success);
        builder.setMessage(R.string.profile_creator_save_success_message);
        builder.setCancelable(false); //Don't let them touch out!
        builder.setNegativeButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

//                        card_view_Home.setVisibility(View.VISIBLE);
//                        linearFooter.setVisibility(View.VISIBLE);


                        try {
                            getGroupList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            dialogInterface.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        HomeFragment ProfileListAct = new HomeFragment();
//                        Utils.SetFragment(ProfileListAct,mContext , "Explorer");

                    }
                });
       /* builder.setPositiveButton(R.string.profile_creator_save_success_scan_another,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      *//*  Intent intent = new Intent(ProfileCreatorActivity.this,
                                CameraReaderActivity.class);
                        startActivity(intent);
                        finish();*//*
                    }
                });
        */

        builder.create().show();
    }

    private void confirmRescan(){
        dialogConfirm(R.string.profile_creator_confirm_rescan,
                R.string.profile_creator_button_rescan,
                CameraReaderActivity.class);
    }

    private void confirmExit(){
        dialogConfirm(R.string.profile_creator_confirm_exit,
                R.string.profile_creator_button_exit,
                MainActivity.class);
    }


    private void dialogConfirm(int dialogMessage,
                               int confirmMessage,
                               final Class newActivityClass){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.dialog_confirmation);
        builder.setMessage(dialogMessage);
        builder.setNegativeButton(R.string.dialog_cancel,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        builder.setPositiveButton(confirmMessage,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      /*  Intent intent = new Intent(ProfileCreatorActivity.this,
                                newActivityClass);
                        startActivity(intent);
                        finish();*/
                    }
                });
        builder.show();
    }

    private void popUpCandidates(Collection<String> candidates, final EditText input){
        if (!candidates.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            final CharSequence[] items = candidates.toArray(new CharSequence[candidates.size()]);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInteface, int i) {
                    input.setText(items[i]);
                }
            });
            builder.show();
        }
    }



    ArrayList<GroupCards>   groupList;

    public void getGroupList() {

        selectedGroupId = new ArrayList<>();
        groupList = new ArrayList<GroupCards>();
        grouNameMatching = new ArrayList<>();

        Cursor groupData = profileDao.loadDataForGroup();

        if (groupData == null) {

            Utils.displayErrorDialog(mContext);

        } else {

            for (groupData.moveToFirst(); !groupData.isAfterLast(); groupData.moveToNext()) {

                try {


                   /* result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                            result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Date))
                            */

                    // Profile profile = new Profile("" + profileData.getString(0), "" + profileData.getString(1), "" + profileData.getString(2), "" + profileData.getString(4), "" + profileData.getString(5), "" + profileData.getString(3), "" + profileData.getString(6), "" + profileData.getString(7),"" + profileData.getString(8));

                    //GroupCards profile = new GroupCards("" + groupData.getString(0), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_NAME)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_COMPANY)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_EMAIL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_WEB)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Address)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Date)));

                    //Here we get Actule Dataas
                    //ArrayList<Profile>  getLiveProfils = profileDao.loadGroupCount( groupData.getString(0));

                    ArrayList<Profile>  getLiveProfils = new ArrayList<>();
                    GroupCards group = new GroupCards("" + groupData.getString(0), "" + groupData.getString(1), getLiveProfils);

                    groupList.add(group);

                    String groupname = groupData.getString(1);

                    //grpdata.add(groupname);

                    grouNameMatching.add(groupname);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            Log.d(ProfileListActivity.class.getName(), "Found " + profileItems.size() + " profiles in list");

            //Setup Listview.

            mLayoutManager = new LinearLayoutManager(mContext);
            mLayoutManager.setOrientation(RecyclerView.VERTICAL);
            list_of_new_group.setLayoutManager(mLayoutManager);
            list_of_new_group.setItemAnimator(new DefaultItemAnimator());

            grpadpter = new GroupAdapter(mContext, groupList, new GetGroupId() {
                @Override
                public void getGId(String gId,boolean isChecked) {


                    if(isChecked){

                        if(!selectedGroupId.contains(gId)){
                            selectedGroupId.add(gId);
                        }
//                        Toast.makeText(mContext,""+gId,Toast.LENGTH_LONG).show();

                    }else{

                        if(selectedGroupId.contains(gId)){
                            selectedGroupId.remove(gId);
                        }

//                        Toast.makeText(mContext,""+gId,Toast.LENGTH_LONG).show();

                    }


                }
            });
            list_of_new_group.setAdapter(grpadpter);

        }

    }

    private void CreateGroup(){

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Group");
        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);
        // add a button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // send data from the AlertDialog to the Activity
                EditText editText = customLayout.findViewById(R.id.editText);
//                sendDialogDataToActivity(editText.getText().toString());
                String groupName = editText.getText().toString();
                try {
                    validateAndCreateGroup(groupName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean saveGroup(GroupCards objgroup) {
        return profileDao.insertGroup(objgroup);
    }

    private void validateAndCreateGroup(String groupName){


        String gId="";
        /*for(int m=0;m<selectedGroupId.size();m++){
            gId += selectedGroupId.get(m)+",";
        }*/


//        Toast.makeText(m)
        ArrayList<Profile>  getLiveProfils = new ArrayList<>();
        GroupCards groupCard = new GroupCards("gid",groupName,getLiveProfils );
        if (groupCard.isValid()){
            if (saveGroup(groupCard)) {

                try {
                    getGroupList();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                showGroupSaveSuccessDialog();

            } else {
                Utils.displayErrorDialog(mContext);
            }
        } else {
            alertInvalidProfile();
        }
    }


    //------------------------------------------Save ImageProcess---------------------Code

    ProgressDialog p;
    private class AsyncTaskSave extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            p = new ProgressDialog(mContext);
            p.setMessage("Please wait Save..");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();

            //card_view_Home.setVisibility(View.GONE);

        }
        @Override
        protected String doInBackground(String... strings) {

            DoInBackgroundImage();

            return "";
        }
        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");

            try {
                p.hide();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                validateAndCreateProfile();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    FileOutputStream fo;
    private String FLASH_MODE;
    private int QUALITY_MODE = 0;
    private void DoInBackgroundImage(){

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (ImageCaptureBitmap != null && QUALITY_MODE == 0)
            ImageCaptureBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        else if (ImageCaptureBitmap != null && QUALITY_MODE != 0)
            ImageCaptureBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY_MODE, bytes);

        File imagesFolder = new File(
                Environment.getExternalStorageDirectory(), Utils.FOLDER_BIZGALLERY);
        if (!imagesFolder.exists())
            imagesFolder.mkdirs(); // <----
        File image = new File(imagesFolder, "BizImg"+System.currentTimeMillis()
                + ".jpg");

        // write the bytes in file

        try {
            FullImagePath = image.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            fo = new FileOutputStream(image);
        } catch (FileNotFoundException e) {
            Log.e("TAG", "FileNotFoundException", e);
            // TODO Auto-generated catch block
        }
        try {
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            Log.e("TAG", "fo.write::PictureTaken", e);
            // TODO Auto-generated catch block
        }

        // remember close de FileOutput
        try {
            fo.close();
            if (Build.VERSION.SDK_INT < 19)
                mContext.sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://"
                                + Environment.getExternalStorageDirectory())));
            else {
                MediaScannerConnection
                        .scanFile(
                               mContext,
                                new String[] { image.toString() },
                                null,
                                new MediaScannerConnection.OnScanCompletedListener() {
                                    public void onScanCompleted(
                                            String path, Uri uri) {
                                        Log.i("ExternalStorage", "Scanned "
                                                + path + ":");
                                        Log.i("ExternalStorage", "-> uri="
                                                + uri);
                                    }
                                });
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    //==========================================================Save Asynck Task End Prcess===========



    //----------------------------------------------AsynckTask ProfileView Details=====================

    ProgressDialog pv;
    private class AsyncTaskProfileview extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pv = new ProgressDialog(mContext);
            pv.setMessage("Please wait Card Data set..");
            pv.setIndeterminate(false);
            pv.setCancelable(false);
            pv.show();

            //card_view_Home.setVisibility(View.GONE);

        }
        @Override
        protected String doInBackground(String... strings) {

            DoInBackGeoundProfileDataSet();


            ((AppCompatActivity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setUp();
                }
            });
            

            return "";
        }
        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");

            try {
                pv.hide();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }



    private void DoInBackGeoundProfileDataSet(){


        ((AppCompatActivity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {


                try {
                    strSetting = getGroupSetting();
//            Toast.makeText(mContext,""+strSetting,Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!generateProfile()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(R.string.profile_creator_alert_read_fail);
                    builder.setNegativeButton(R.string.profile_creator_alert_read_fail_manual,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    builder.show();

                }

            }
        });

    }


    public String getGroupSetting() {


        String strSetting = "False";
        Cursor groupData = profileDao.GroupSetting();

        if (groupData == null) {

            Utils.displayErrorDialog(mContext);

        } else {

            for (groupData.moveToFirst(); !groupData.isAfterLast(); groupData.moveToNext()) {
                try {
//                    strId = groupData.getString(0);
                    strSetting = groupData.getString(1);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        return  strSetting;
    }

}