package com.bizcards.act;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.AsyncHttpResponseHandler;
//import com.loopj.android.http.RequestParams;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import info.androidhive.barcode.BarcodeReader;
//import jeevis.mini.api.ver.utility.Constant;

//import static jeevis.mini.api.ver.MainActivity.IME_Device;


public class ScanActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    BarcodeReader barcodeReader;
    String Str_barcode;
    Context mContext;

    public static  boolean isScanActivity =false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_scan);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = ScanActivity.this;
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
    }

    @Override
    public void onScanned(Barcode barcode) {

        // playing barcode reader beep sound
       /* try {
            barcodeReader.playBeep();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {

            Str_barcode = barcode.displayValue;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    RegDeviceScanApi();
                    try {

                         isScanActivity = true;
                         Intent mainIntent = new Intent(ScanActivity.this, MainActivity.class);
                         mainIntent.putExtra("Resonse",Str_barcode);
                         startActivity(mainIntent);

                        // on Request to just scan one item
                        barcodeReader.onPause();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        // search the barcode
        ///searchBarcode(Str_barcode);
       /* try {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(ScanActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(ScanActivity.this);
            }
            builder.setTitle("Delete entry")
                    .setMessage(""+Str_barcode)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(ScanActivity.this, MainActivity.class);
                            intent.putExtra("code", Str_barcode);
                            startActivity(intent);
                            finish();

                            // continue with delete
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

        // ticket details activity by passing barcode
       /* Intent intent = new Intent(ScanActivity.this, TicketResultActivity.class);
        intent.putExtra("code", barcode.displayValue);
        startActivity(intent);*/

        // For Camera Process


        /*if (checkPermission(wantCameraPermission)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(Environment.getExternalStorageDirectory(),"test.jpg");
            outputFileUri = Uri.fromFile(file);
            Log.d("TAG", "outputFileUri intent" + outputFileUri);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, 111);

        } else {
            requestPermission(wantCameraPermission, PERMISSION_REQUEST_CODE_Camera);
        }*/

    }



   /*private void searchBarcode(String barcode) {
        // making volley's json request
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL + barcode, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "Ticket response: " + response.toString());

                        // check for success status
                        if (!response.has("error")) {
                            // received movie response
                            renderMovie(response);
                        } else {
                            // no movie found
                            showNoTicket();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                showNoTicket();
            }
        });

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
    }*/


    @Override
    public void onScannedMultiple(List<Barcode> list) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String s) {
        Toast.makeText(getApplicationContext(), "Error occurred while scanning " + s, Toast.LENGTH_SHORT).show();
    }

    @Override

    public void onCameraPermissionDenied() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PERMISSION_REQUEST_CODE_Camera = 2;
    String wantPermission = Manifest.permission.READ_EXTERNAL_STORAGE;
    String wantCameraPermission = Manifest.permission.CAMERA;

    Uri outputFileUri;

    private boolean checkPermission(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(mContext, permission);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                return false;
            }
        } else {
            return true;
        }
    }

    private void requestPermission(String permission, int permission_requestcode) {


        //FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},permission_requestcode);

        /*if (ActivityCompat.shouldShowRequestPermissionRationale((AppCompatActivity) mcontext, permission)) {
            Toast.makeText(mcontext, "Read external storage permission allows us to write data.Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        }*/

//        ActivityCompat.requestPermissions((AppCompatActivity) mcontext, new String[]{permission}, permission_requestcode);

        requestPermissions(new String[]{permission}, permission_requestcode);
    }


    /*public void RegDeviceScanApi() {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("Flag", "SpApiAdvQrLgnVerify");
        params.put("FlagType", "IMEQRCHECK");
        params.put("AppCode", "JWCPTNAPP");
        params.put("Form", "JWCQRSLGN");
//        params.put("USRIME", "" + "000000000000000");
        params.put("USRIME", ""+IME_Device);
        //mLatitude,mLongitude
        params.put("USRNETLG", ""+MainActivity.mLatitude);
        params.put("USRNETLT", ""+MainActivity.mLongitude);
//        params.put("USRNETLG", "72.12");
//        params.put("USRNETLT", "13.455");
//        params.put("USRNETTL", "navarnagpura C G Road");
        params.put("USRNETTL", ""+MainActivity.mAddress);
        params.put("USRNETIP", ""+Login.mIP);
        params.put("USRQRID", ""+Str_barcode);
        client.get(Constant.DOMAINPATIENTS + "" + Constant.APIQR, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, cz.msebera.android.httpclient.Header[] headers, byte[] bytes) {
                String result = (new String(bytes));
                Log.i("Ime", "Value--------->" + result);
                if (result.equalsIgnoreCase("[{\"Column1\":\"SeRR 1\"}]")) {
                    //Invalid Mobile Number or OTP : Contact your System Administrator
                    try {
                        Toast.makeText(ScanActivity.this, "Login Rejected  !", Toast.LENGTH_LONG).show();

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result","Login Rejected  !");
                        setResult(AppCompatActivity.RESULT_CANCELED, returnIntent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // USRCNN=100; then
                    //Go to Password screen;
                    try {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result","QR:" + Str_barcode + "\nRespones :" + result+"\n IME:"+IME_Device);
                        setResult(AppCompatActivity.RESULT_OK,returnIntent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers, byte[] bytes, Throwable throwable) {

                String result = (new String(bytes));

                Toast.makeText(ScanActivity.this, "No Data Found !" + result, Toast.LENGTH_LONG).show();
            }

            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }
        });
    }*/

}
