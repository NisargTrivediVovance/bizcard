package com.bizcards.act;

import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bizcards.act.pojo.GroupCards;
import com.bizcards.act.ui.send.SendViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import static com.bizcards.act.MainActivity.drawer;

public class Settings  extends Fragment {

    Context  mContext;
    ImageView menu_settings;


    ProfileDao profileDao;

    Switch switchOnOff;

    String strSetting,strId;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.settings, container, false);

        mContext = Settings.this.getActivity();
        profileDao = ProfileDao.getInstance(mContext);

        try {
            strSetting = getGroupSetting();
//            Toast.makeText(mContext,""+strSetting,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


        switchOnOff = (Switch) root.findViewById(R.id.switchOnOff);
        menu_settings = (ImageView)root.findViewById(R.id.menu_settings);

        if(strSetting.equalsIgnoreCase("true")){
            switchOnOff.setChecked(true);
        }else {
            switchOnOff.setChecked(false);
        }


        switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b){
                    profileDao.updateGroupSetting("true","1");
//                    Toast.makeText(mContext,"Switch-"+b+"-"+strId,Toast.LENGTH_LONG).show();
                }else{
                    profileDao.updateGroupSetting("false","1");
//                    Toast.makeText(mContext,"Switch-"+b,Toast.LENGTH_LONG).show();
                }
            }
        });


        menu_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        return root;

 }



    public String getGroupSetting() {


        String strSetting = "False";
        Cursor groupData = profileDao.GroupSetting();

        if (groupData == null) {

            Utils.displayErrorDialog(mContext);

        } else {

            for (groupData.moveToFirst(); !groupData.isAfterLast(); groupData.moveToNext()) {
                try {
                    strId = groupData.getString(0);
                    strSetting = groupData.getString(1);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        return  strSetting;
    }

}
