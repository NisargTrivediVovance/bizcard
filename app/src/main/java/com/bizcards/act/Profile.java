package com.bizcards.act;

import android.text.Editable;

import org.apache.commons.lang3.StringUtils;

import java.sql.Blob;

/**
 * Created by Koha Choji on 06/06/2017.
 */

public class Profile {

    public void setId(String id) {
        this.id = id;
    }

    private String id;
    private String name;
    private String jobTitle;
    private String company;
    private String primaryContactNumber;
    private String secondaryContactNumber;
    private String email;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    private String groupId;

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    private String isFavorite;


    public String getStrWeb() {
        return strWeb;
    }

    public void setStrWeb(String strWeb) {
        this.strWeb = strWeb;
    }

    private String strWeb;

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }

    private String strAddress;

    public String getStrDate() {
        return strDate;
    }


    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    private String strDate;


    public  byte[]  getImageBlob() {
        return imageBlob;
    }

    public void setImageBlob( byte[]  imageBlob) {
        this.imageBlob = imageBlob;
    }

    public  byte[]  imageBlob;

    public String getImagepath() {
        return Imagepath;
    }

    public void setImagepath(String imagepath) {
        Imagepath = imagepath;
    }

    public String Imagepath;

    public Profile(String mid,String name,
                   String jobTitle,
                   String company,
                   String primaryContactNumber,
                   String email, String mstrWeb, String mstrAddress, String mstrDate, String strFav,String mgroupId, byte[]   mImageBlob,String mImagePath) {

        this.id = mid;
        this.name = name;
        this.jobTitle = jobTitle;
        this.company = company;
        this.primaryContactNumber = primaryContactNumber;
        this.email = email;
        this.strWeb = mstrWeb;
        this.strAddress = mstrAddress;
        this.strDate = mstrDate;
        this.isFavorite = strFav;
        this.groupId = mgroupId;
        this.imageBlob = mImageBlob;
        this.Imagepath = mImagePath;

    }

    public boolean isValid() {
        boolean isValid = StringUtils.isNotBlank(name)
                && StringUtils.isNotBlank(company);
        return isValid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPrimaryContactNumber() {
        return primaryContactNumber;
    }

    public void setPrimaryContactNumber(String primaryContactNumber) {
        this.primaryContactNumber = primaryContactNumber;
    }

    public String getSecondaryContactNumber() {
        return secondaryContactNumber;
    }

    public void setSecondaryContactNumber(String secondaryContactNumber) {
        this.secondaryContactNumber = secondaryContactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getId() {
        return id;
    }

   /* public void setId(String id) {
        this.id = id;
    }*/

}
