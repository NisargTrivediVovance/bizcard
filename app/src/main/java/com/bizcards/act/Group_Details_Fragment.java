package com.bizcards.act;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bizcards.act.adapter.FavoriteAdapter;
import com.bizcards.act.adapter.GroupDetailsAdapter;
import com.bizcards.act.interfaceAll.GetDeleteId;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.bizcards.act.MainActivity.drawer;
//import static com.bizcards.act.MainActivity.linearProfile_details;
import static com.bizcards.act.adapter.GroupFragAdapter.StrGroupName;
import static com.bizcards.act.adapter.GroupFragAdapter.mGProfile;

public class Group_Details_Fragment extends Fragment {

    Context mContext;

    ImageButton img_plus;

    //CardView  card_view8;

    RecyclerView list_of_new_group;

    LinearLayoutManager mLayoutManager;

    GroupDetailsAdapter favoriteadp;

    public static ImageView imgMenu_group;

    TextView txtgroup;
    ImageView img_Settings;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favourites, container, false);

        mContext = Group_Details_Fragment.this.getActivity();
//        img_plus = (ImageButton) root.findViewById(R.id.img_plus);
        list_of_new_group = (RecyclerView) root.findViewById(R.id.list_of_new_group);
        imgMenu_group = (ImageView)root.findViewById(R.id.imgMenu_group);

        img_Settings = (ImageView)  root.findViewById(R.id.img_Settings);


        txtgroup = (TextView)root.findViewById(R.id.txtgroup) ;

        txtgroup.setText(StrGroupName);

        imgMenu_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });


        img_Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Settings objSetting = new Settings();
                Utils.SetFragment(objSetting, mContext, "Settings");

            }
        });


        return root;
    }


    ArrayList<Profile> mFavoriteList;

    @Override
    public void onResume() {
        super.onResume();


         mFavoriteList = mGProfile;

//        boolean isupdate = profileDao.update(profile);

        setUp(mFavoriteList);


        //linearProfile_details.setVisibility(View.GONE);
    }

    private void setUp(ArrayList<Profile>  getlist) {

        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        list_of_new_group.setLayoutManager(mLayoutManager);
        list_of_new_group.setItemAnimator(new DefaultItemAnimator());

//        ArrayList<String> grpdata = new ArrayList<>();
//        grpdata.add("Business");
//        grpdata.add("Family");
//        grpdata.add("Vip");
//        grpdata.add("Colleagues");
//        grpdata.add("Customer");

        favoriteadp = new GroupDetailsAdapter(mContext, getlist, new GetDeleteId() {
            @Override
            public void getMDeleteId(Profile Id) {
                DeleteProcess(Id);
            }
        });
        list_of_new_group.setAdapter(favoriteadp);
        // prepareDemoContent();
    }

    private   void DeleteProcess(Profile mId){


        new AlertDialog.Builder(mContext)
                .setTitle("Delete Card Entry")
                .setMessage("Are you sure you want to delete this Card entry?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(R.string.dialog_Yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation


                        ProfileDao profileDao = null;
                        boolean isDelte = false;
                        try {
                            profileDao = ProfileDao.getInstance(mContext);

//                            int mId = Integer.parseInt(childText.getId());
                            isDelte = profileDao.deleteCard(Integer.parseInt(mId.getId()));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        if (isDelte) {
                            Toast.makeText(mContext, "Card Deleted Successful !", Toast.LENGTH_SHORT).show();

                            try {
                                mFavoriteList.remove(mId);

                                setUp(mFavoriteList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

//                            Favourites_Fragment.AsyncFavorite asynckTask =  new Favourites_Fragment.AsyncFavorite();
//                            asynckTask.execute("");
//                            listDataChild.remove(mId);
//                            try {
//                                adpMainExpandable.notifyDataSetChanged();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }


                        } else {
                            Toast.makeText(mContext, "Card could not Deleted Successful !", Toast.LENGTH_SHORT).show();
                        }

                        try {
                            profileDao.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(R.string.dialog_No, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }
}
