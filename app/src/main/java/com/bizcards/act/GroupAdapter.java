package com.bizcards.act;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bizcards.act.interfaceAll.GetGroupId;
import com.bizcards.act.pojo.GroupCards;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class GroupAdapter extends RecyclerView.Adapter<GroupViewHolder> {

    private static final String TAG = "SportAdapter";
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;
    private Callback mCallback;

    Context context;
    ArrayList<GroupCards> newgrouplist;
    GetGroupId   objGroupId;

    public void  SetGroupId(GetGroupId   mGI){
        objGroupId =  mGI;
    }

    //ArrayList<GroupCards>   groupList
    public GroupAdapter (Context mcontext, ArrayList<GroupCards> grouplist,GetGroupId mObjGoup){
        context = mcontext;
        newgrouplist = grouplist;
        objGroupId = mObjGoup;
    }

   // private List<Sport> mSportList;
  /*  public SportAdapter(List<Sport> sportList) {
        mSportList = sportList;
    }*/
    public void setCallback(Callback callback) {
        mCallback = callback;
    }
    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_group_raw, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_empty_view, parent, false));
        }
    }
    @Override
    public int getItemViewType(int position) {
        if (newgrouplist != null && newgrouplist.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }
    @Override
    public int getItemCount() {
        if (newgrouplist != null && newgrouplist.size() > 0) {
            return newgrouplist.size();
        } else {
            return 1;
        }
    }
   /* public void addItems(List<Sport> sportList) {
        mSportList.addAll(sportList);
        notifyDataSetChanged();
    }*/
    public interface Callback {
        void onEmptyViewRetryClick();
    }
    public class ViewHolder extends GroupViewHolder {
       // @BindView(R.id.thumbnail)
        /*ImageView coverImageView;
        @BindView(R.id.title)
        TextView titleTextView;
        @BindView(R.id.newsTitle)
        TextView newsTextView;
        @BindView(R.id.newsInfo)
        TextView infoTextView;*/
        CheckBox chkbox_group;
        TextView txt_newgroup;

        public ViewHolder(View itemView) {
            super(itemView);
           // ButterKnife.bind(this, itemView);
            chkbox_group = (CheckBox)itemView.findViewById(R.id.chkbox_group);
            txt_newgroup = (TextView)itemView.findViewById(R.id.txt_newgroup);
        }
        protected void clear() {
            //coverImageView.setImageDrawable(null);
            txt_newgroup.setText("");
            //infoTextView.setText("");
        }
        public void onBind(int position) {
            super.onBind(position);


            GroupCards getgrp_name = newgrouplist.get(position);

            txt_newgroup.setText(getgrp_name.getName());

            chkbox_group.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    GroupCards getgrp_Name = newgrouplist.get(position);

                    if(chkbox_group.isChecked()){
                       // objGroupId()
                        String Gid = getgrp_Name.getGroup_id();
                        objGroupId.getGId(Gid,true);

                    }else{

                        String Gid =getgrp_Name.getGroup_id();
                        objGroupId.getGId(Gid,false);

                    }
                }
            });

           /* final Sport mSport = mSportList.get(position);
            if (mSport.getImageUrl() != null) {
                Glide.with(itemView.getContext())
                        .load(mSport.getImageUrl())
                        .into(coverImageView);
            }*/
           /* if (mSport.getTitle() != null) {
                titleTextView.setText(mSport.getTitle());
            }
            if (mSport.getSubTitle() != null) {
                newsTextView.setText(mSport.getSubTitle());
            }
            if (mSport.getInfo() != null) {
                infoTextView.setText(mSport.getInfo());
            }*/
           /* itemView.setOnClickListener(v -> {
                if (mSport.getImageUrl() != null) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse(mSport.getImageUrl()));
                        itemView.getContext().startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG, "onClick: Image url is not correct");
                    }
                }
            });*/
        }
    }
    public class EmptyViewHolder extends GroupViewHolder {
       /* @BindView(R.id.tv_message)
        TextView messageTextView;
        @BindView(R.id.buttonRetry)
        TextView buttonRetry;*/
        EmptyViewHolder(View itemView) {
            super(itemView);
            //ButterKnife.bind(this, itemView);
            //buttonRetry.setOnClickListener(v -> mCallback.onEmptyViewRetryClick());
        }
        @Override
        protected void clear() {
        }



    }





}