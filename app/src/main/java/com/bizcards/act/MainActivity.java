package com.bizcards.act;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//
//import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import com.google.android.material.navigation.NavigationView;
//import com.google.android.material.snackbar.Snackbar;
//
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//import androidx.drawerlayout.widget.DrawerLayout;
//import androidx.navigation.NavController;
//import androidx.navigation.Navigation;
//import androidx.navigation.ui.AppBarConfiguration;
//import androidx.navigation.ui.NavigationUI;


import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.bizcards.act.Utilities.CameraPreview;
import com.bizcards.act.adapter.ExpandableList_Home;
import com.bizcards.act.adapter.NavAdapter;
import com.bizcards.act.interfaceAll.GetNavId;
//import com.bizcards.act.ui.home.HomeFragment;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.bizcards.act.Manual_Profile_Creator_Fragment.PICK_IMAGE_ID;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Address;
import static com.bizcards.act.ProfileDao.PROFILE_COL_COMPANY;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Date;
import static com.bizcards.act.ProfileDao.PROFILE_COL_EMAIL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_GroupId;
import static com.bizcards.act.ProfileDao.PROFILE_COL_ImagePath;
import static com.bizcards.act.ProfileDao.PROFILE_COL_IsFavorite;
import static com.bizcards.act.ProfileDao.PROFILE_COL_JOB_TITLE;
import static com.bizcards.act.ProfileDao.PROFILE_COL_NAME;
import static com.bizcards.act.ProfileDao.PROFILE_COL_PRIMARY_TEL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_WEB;
import static com.bizcards.act.ScanActivity.isScanActivity;

public class MainActivity extends AppCompatActivity implements ExpandableListView.OnChildClickListener {

    private static final String TAG = "MainActivity";
    private Button profileListButton;
    private Button cameraReaderButton;


//    private AppBarConfiguration mAppBarConfiguration;

/*

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
*/


    public static FragmentManager masterFragmentManager1;


    Context mContext;

    //ForCamera Process;

    //    public static SurfaceView cameraView;
    SurfaceHolder surfaceHolder;
    Camera camera = null;
    private Camera.PictureCallback mPicture;
    FrameLayout Surfacefrmlayout;


//    public static CameraSource cameraSource;

    private TextView textView, tvScanimg;
    private final int REQUEST_CAMERA_PERMISSION_ID = 1001;
    //    private final String TAG = "CameraReaderActivity";
    public static final String PROFILE_DATA_KEY = "profile_data_key";
    private final int CAMERA_TIME = 10000;
    private List<String> profile_data = new ArrayList<String>();
    private Handler handler = new Handler();


    TextView tvScan, tvCreateProfile, groups_fragment;
//    public static LinearLayout linearProfile_details;


    public static LinearLayout linearFooter;
//    EditText input_website;


    public static Bitmap ImageCaptureBitmap = null;
    public static String FullImagePath = "";

    private Camera mCamera;
    private boolean cameraFront = false;


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION_ID:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_PERMISSION_ID);
                        return;
                    }
                   /* try {
//                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                }
                break;
            default:
                break;
        }
    }

    FloatingActionButton fab;

//    public static ImageView imgMenu;


    public static DrawerLayout drawer;
    ExpandableListView drawerList;
//    LinearLayout linear1;


    //    public static CardView card_view_Home;
    TextView tvScanQr, groups_fragmentimg;


    TextView tvFavorite;


    String[] permissions;


    // Intent i = new Intent(Login.this, ScanActivity.class);
    //                startActivityForResult(i, 1);
    public static final int MULTIPLE_PERMISSIONS = 1;
    ImageButton gellryimgbtn, imgcaptbtn;


    String MTAG = "MainActivity-";


    LinearLayout linearScanQr, linearCreate, linearScanCard, linearGroup, lnrFavorite;


    ProfileDao profileDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        mContext = MainActivity.this;
        profileDao = ProfileDao.getInstance(mContext);





        Log.i(MTAG, " Load..1");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            permissions = new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,

            };

        } else {

            permissions = new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
            };

        }


        Log.i(MTAG, " Load..2");
        try {
            checkPermissions();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            cameraPreview = (LinearLayout) findViewById(R.id.cPreview);
            mPreview = new CameraPreview(mContext, mCamera);
            cameraPreview.addView(mPreview);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i(MTAG, " Load..3");

        tvScanQr = (TextView) findViewById(R.id.tvScanQr);
        gellryimgbtn = (ImageButton) findViewById(R.id.gellryimgbtn);
        imgcaptbtn = (ImageButton) findViewById(R.id.imgcaptbtn);

        imgcaptbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(intent, 7);
                // camera.takePicture(null, null, mPicture);
                // cameraSource.takePicture(null, null, mPicture);


                try {
                    mPicture = getPictureCallback();
                    mCamera.takePicture(null, null, mPicture);

                } catch (Exception e) {
                    e.printStackTrace();
                }
//                takeImage();
//                sdvdsvd
            }
        });


//        input_website = (EditText) findViewById(R.id.input_website);

        //imgMenu = (ImageView) findViewById(R.id.imgMenu);
//        card_view_Home = (CardView) findViewById(R.id.card_view_Home);
//        linear1 = (LinearLayout)findViewById(R.id.linear1);
//        linearProfile_details = (LinearLayout) findViewById(R.id.linearProfile_details);

        Surfacefrmlayout = (FrameLayout) findViewById(R.id.Surfacefrmlayout);
        linearFooter = (LinearLayout) findViewById(R.id.linearFooter);
        tvScanimg = (TextView) findViewById(R.id.tvScanimg);
        textView = (TextView) findViewById(R.id.text_view);
        tvScan = (TextView) findViewById(R.id.tvScan);
        groups_fragmentimg = (TextView) findViewById(R.id.groups_fragmentimg);

        tvCreateProfile = (TextView) findViewById(R.id.tvCreateProfile);
        groups_fragment = (TextView) findViewById(R.id.groups_fragment);

        linearScanQr = (LinearLayout) findViewById(R.id.linearScanQr);
        linearCreate = (LinearLayout) findViewById(R.id.linearCreate);
        linearScanCard = (LinearLayout) findViewById(R.id.linearScanCard);
        linearGroup = (LinearLayout) findViewById(R.id.linearGroup);

        lnrFavorite = (LinearLayout) findViewById(R.id.lnrFavorite);
        tvFavorite = (TextView) findViewById(R.id.tvFavorite);

        masterFragmentManager1 = getSupportFragmentManager();

        fab = findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);

        String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                newString = null;
            } else {

                if (isScanActivity) {

//                    card_view_Home.setVisibility(View.GONE);
                    linearFooter.setVisibility(View.GONE);
                    fab.setVisibility(View.GONE);
                }

                newString = extras.getString("Resonse");
                if (!TextUtils.isEmpty(newString)) {

                    profile_data.add(newString);
//                Toast.makeText(mContext, "Response--" + newString, Toast.LENGTH_LONG).show();

                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList(PROFILE_DATA_KEY, (ArrayList<String>) profile_data);
//                bundle.setArguments(bundle);

                    ProfileCreatorFragment profileList = new ProfileCreatorFragment();
                    profileList.setArguments(bundle);
                    Utils.SetFragment(profileList, mContext, "ProfileCreator");
                }

            }
        } else {

            newString = (String) savedInstanceState.getSerializable("STRING_I_NEED");

        }


        gellryimgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, PICK_IMAGE_ID);
            }
        });


        linearScanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this, ScanActivity.class);
                startActivityForResult(i, 1);
            }
        });


        tvScanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this, ScanActivity.class);
                startActivityForResult(i, 1);
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = findViewById(R.id.drawer_layout);
        drawerList = (ExpandableListView) findViewById(R.id.left_drawer);


        //=================================Expandable data start ====

        // preparing list data
        prepareListData();

        drawerList.setAdapter(new NavAdapter(this, listDataHeader, listDataChild, new GetNavId() {
            @Override
            public void getNevId(int groupPosition) {

                if (groupPosition == 0) {

//                    card_view_Home.setVisibility(View.VISIBLE);
//                    HomeFragment ProfileListAct = new HomeFragment();
//                    Utils.SetFragment(ProfileListAct, mContext, "HomeFragment");

                    MasterHomeFrgament master = new MasterHomeFrgament();
                    Utils.SetFragment(master, mContext, "HomeFragment");


                } else if (groupPosition == 1) {

                } else if (groupPosition == 2) {


                } else if (groupPosition == 3) {
                    Settings objSetting = new Settings();
                    Utils.SetFragment(objSetting, mContext, "Settings");

                } else if (groupPosition == 4) {

                } else if (groupPosition == 5) {
                }


                drawer.closeDrawer(GravityCompat.START);

            }
        }));


        drawerList.setOnChildClickListener(this);
        drawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {


//                if (groupPosition != 1) {
                // drawer.closeDrawer(GravityCompat.START);
//                }


                if (groupPosition == 0) {


                } else if (groupPosition == 1) {

                } else if (groupPosition == 2) {

                } else if (groupPosition == 3) {

                } else if (groupPosition == 4) {

                } else if (groupPosition == 5) {
                }


                return false;
            }
        });


        //=============================Expandable  End

        //NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
       /* mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);*/

//
        //      toolbar.setTitle("");
        //    setSupportActionBar(toolbar);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

       /* toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(MainActivity.this, "Back clicked!",     Toast.LENGTH_SHORT).show();



                masterFragmentManager1.popBackStack();

                int mcount = masterFragmentManager1.getBackStackEntryCount();
                if(mcount==2) {
                    imgMenu.setVisibility(View.VISIBLE);
                    toolbar.setNavigationIcon(null);
                }

//                onBackPressed();

              *//*  if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }*//*
            }
        });

*/
      /*  imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backProcess_with_Click_Home();

            }
        });
*/






       /* profileListButton = (Button) findViewById(R.id.profileListButton);
        cameraReaderButton = (Button) findViewById(R.id.cameraReaderButton);

        profileListButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfileListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        cameraReaderButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CameraReaderActivity.class);
                startActivity(intent);
            }
        });
*/


        tvScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intent intent = new Intent(mContext, CameraReaderActivity.class);
                //startActivity(intent);

//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                getSupportActionBar().setDisplayShowHomeEnabled(true);

                linearFooter.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //  ScanProcess();

                        MainCreateCamera();
                    }
                });


//                surfaceHolder = cameraView.getHolder();
//                surfaceHolder.addCallback(mContext);
//                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//                ScanProcess();

//                if(!previewing){
                   /* camera = Camera.open();
                    if (camera != null){
                        try {
                            camera.setPreviewDisplay(surfaceHolder);
                            camera.startPreview();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }*/
//                }



               /* if(camera == null) {
                    camera = Camera.open();
                    camera.setDisplayOrientation(90);

                    try {
                        camera.setPreviewDisplay(surfaceHolder);
                        camera.startPreview();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    mPicture = getPictureCallback();


//                    mPreview.refreshCamera(mCamera);
                    Log.d("nu", "null");
                }else {
                    Log.d("nu","no null");
                }*/
            }


//            boolean nn = openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);

        });



        linearScanCard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intent intent = new Intent(mContext, CameraReaderActivity.class);
                //startActivity(intent);

//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                getSupportActionBar().setDisplayShowHomeEnabled(true);

                linearFooter.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);

//                cameraView.setVisibility(View.VISIBLE);
//                Surfacefrmlayout.setVisibility(View.VISIBLE);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //  ScanProcess();

                        MainCreateCamera();
                    }
                });
            }
        });





        tvScanimg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intent intent = new Intent(mContext, CameraReaderActivity.class);
                //startActivity(intent);

//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                getSupportActionBar().setDisplayShowHomeEnabled(true);

                linearFooter.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);

//                cameraView.setVisibility(View.VISIBLE);
//                Surfacefrmlayout.setVisibility(View.VISIBLE);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //  ScanProcess();

                        MainCreateCamera();
                    }
                });
            }
        });




        linearGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                card_view_Home.setVisibility(View.GONE);
                //linearProfile_details.setVisibility(View.VISIBLE);

                Groups_Fragment groupsfragment = new Groups_Fragment();
                Utils.SetFragment(groupsfragment, mContext, "Groups Frgment");
            }
        });

        groups_fragmentimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                card_view_Home.setVisibility(View.GONE);
                //linearProfile_details.setVisibility(View.VISIBLE);

                Groups_Fragment groupsfragment = new Groups_Fragment();
                Utils.SetFragment(groupsfragment, mContext, "Groups Frgment");
            }
        });


        tvFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                card_view_Home.setVisibility(View.GONE);
//                linearProfile_details.setVisibility(View.VISIBLE);
                Favourites_Fragment manualFavorite = new Favourites_Fragment();
                Utils.SetFragment(manualFavorite, mContext, "Favorite");

            }
        });

        lnrFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // linear1.setVisibility(View.GONE);
                //  card_view_Home.setVisibility(View.GONE);
                // linearProfile_details.setVisibility(View.VISIBLE);
                Favourites_Fragment manualFavorite = new Favourites_Fragment();
                Utils.SetFragment(manualFavorite, mContext, "Favorite");

            }
        });

        linearCreate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intent intent = new Intent(mContext, CameraReaderActivity.class);
                //startActivity(intent);

//                linear1.setVisibility(View.GONE);
//                card_view_Home.setVisibility(View.GONE);
//                linearProfile_details.setVisibility(View.VISIBLE);
                Manual_Profile_Creator_Fragment manualprofilecrate = new Manual_Profile_Creator_Fragment();
                Utils.SetFragment(manualprofilecrate, mContext, "Profile Creator");


            }
        });


        tvCreateProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intent intent = new Intent(mContext, CameraReaderActivity.class);
                //startActivity(intent);

//                linear1.setVisibility(View.GONE);
//                card_view_Home.setVisibility(View.GONE);
//                linearProfile_details.setVisibility(View.VISIBLE);
                Manual_Profile_Creator_Fragment manualprofilecrate = new Manual_Profile_Creator_Fragment();
                Utils.SetFragment(manualprofilecrate, mContext, "Profile Creator");


            }
        });

        groups_fragment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intent intent = new Intent(mContext, CameraReaderActivity.class);
                //startActivity(intent);

                //linear1.setVisibility(View.GONE);

//                card_view_Home.setVisibility(View.GONE);
                //linearProfile_details.setVisibility(View.VISIBLE);

                Groups_Fragment groupsfragment = new Groups_Fragment();
                Utils.SetFragment(groupsfragment, mContext, "Groups Frgment");


            }
        });

        //=======================Camera


        try {
            Log.i(MTAG, " Load..4");
            //HomeFragment ProfileListAct = new HomeFragment();
            //Utils.SetFragment(ProfileListAct, mContext, "HomeFragment");

            MasterHomeFrgament master = new MasterHomeFrgament();
            Utils.SetFragment(master, mContext, "HomeFragment");

            Log.i(MTAG, " Load..5");
        } catch (Exception e) {
            e.printStackTrace();
        }



        try {
            // Default setting entry From Home we are seted;
            String isSet = getGroupSetting();
            if(TextUtils.isEmpty(isSet)) {
                saveGroupSetting("true");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        if (masterFragmentManager1.getBackStackEntryCount() > 0) {

            masterFragmentManager1.popBackStack();

          /*  int mcount = masterFragmentManager1.getBackStackEntryCount();
            if(mcount==2) {
                imgMenu.setVisibility(View.VISIBLE);
            }*/

            int mcount = masterFragmentManager1.getBackStackEntryCount();
            if (mcount == 2) {
//                imgMenu.setImageResource(R.drawable.menu);
//                linear1.setVisibility(View.VISIBLE);
//                card_view_Home.setVisibility(View.VISIBLE);
                linearFooter.setVisibility(View.VISIBLE);
//                linearProfile_details.setVisibility(View.GONE);

                Surfacefrmlayout.setVisibility(View.GONE);
            }

        } else {
            super.onBackPressed();
//            imgMenu.setVisibility(View.VISIBLE);
            linearFooter.setVisibility(View.VISIBLE);

            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }


    private void backProcess_with_Click_Home() {

        if (masterFragmentManager1.getBackStackEntryCount() > 1) {
            masterFragmentManager1.popBackStack();

            int mcount = masterFragmentManager1.getBackStackEntryCount();

            if (mcount == 2) {
//                imgMenu.setImageResource(R.drawable.menu);
                //linear1.setVisibility(View.VISIBLE);
//                card_view_Home.setVisibility(View.VISIBLE);
//                linearProfile_details.setVisibility(View.GONE);
                linearFooter.setVisibility(View.VISIBLE);
            }

        } else {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        }
    }

/*
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        if (masterFragmentManager1.getBackStackEntryCount() > 0) {
//            masterFragmentManager1.popBackStack();

           *//* int mcount = masterFragmentManager1.getBackStackEntryCount();
            if(mcount==2) {
                imgMenu.setVisibility(View.VISIBLE);
            }*//*

            int mcount = masterFragmentManager1.getBackStackEntryCount();
            if(mcount==2) {
                imgMenu.setImageResource(R.drawable.menu);
            }


        } else {
            //imgMenu.setVisibility(View.VISIBLE);
        }

*//*
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();*//*
    }*/


    public void onResume() {
        super.onResume();

        try {
            if (mCamera == null) {
                mCamera = Camera.open();
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
                Log.d("nu", "null");
            } else {
                Log.d("nu", "no null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    private void ScanProcess() {

//        cameraView = (SurfaceView) findViewById(R.id.surface_view);
//        cameraView.setVisibility(View.VISIBLE);
        Surfacefrmlayout.setVisibility(View.VISIBLE);
        //card_view_Home.setVisibility(View.GONE);
        linearFooter.setVisibility(View.GONE);
        fab.setVisibility(View.GONE);


        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext())
                .build();

       /* cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1280, 1024)
                .setRequestedFps(2.0f)
                .setAutoFocusEnabled(true)
                .build();*/

        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Text recognizer is not operational!");

        } else {

           /* cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setRequestedFps(2.0f)
                    .setAutoFocusEnabled(true)
                    .build();*/


          /*  cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_PERMISSION_ID);
                        return;
                    }

                   *//* try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*//*
                }

                @Override
                public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                    cameraSource.stop();
                }

            });
*/

            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {

                @Override
                public void release() {

                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0) {
                        textView.post(new Runnable() {
                            @Override
                            public void run() {
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < items.size(); i++) {
                                    sb.append(items.valueAt(i).getValue()).append("\n");
                                }
                                String capturedString = sb.toString();
                                textView.setText(capturedString);
                                if (StringUtils.isNotBlank(capturedString)) {
                                    profile_data.add(capturedString);
                                }
                            }
                        });
                    }
                }
            });
        }


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

//                Intent intent = new Intent(CameraReaderActivity.this, ProfileCreatorActivity.class);
//                intent.putStringArrayListExtra(PROFILE_DATA_KEY, (ArrayList<String>) profile_data);
                //  startActivity(intent);

//                cameraView.setVisibility(View.GONE);
//                Surfacefrmlayout.setVisibility(View.GONE);


                try {
                    textRecognizer.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Bundle bundle = new Bundle();
                bundle.putStringArrayList(PROFILE_DATA_KEY, (ArrayList<String>) profile_data);
//                bundle.setArguments(bundle);

//                ProfileCreatorFragment ProfileListAct = new ProfileCreatorFragment();
//                ProfileListAct.setArguments(bundle);
//                Utils.SetFragment(ProfileListAct, mContext, "ProfileCreator");


//                finish();
            }
        }, CAMERA_TIME);


    }


    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    //==========   Expandable Process======================


    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding headers
        Resources res = getResources();
        String[] headers = res.getStringArray(R.array.nav_drawer_labels);
        listDataHeader = Arrays.asList(headers);

        // Static method
        List<String> home, friends, notifs;
        String[] shome, sfriends, snotifs;

        shome = res.getStringArray(R.array.elements_home);
        home = Arrays.asList(shome);

        sfriends = res.getStringArray(R.array.elements_friends);
        friends = Arrays.asList(sfriends);

        snotifs = res.getStringArray(R.array.elements_notifs);
        notifs = Arrays.asList(snotifs);

        //Here we also need to add child values like group added value
        // Add to hashMap
//        listDataChild.put(listDataHeader.get(0), home); // Header, Child data
//        listDataChild.put(listDataHeader.get(1), friends);
//        listDataChild.put(listDataHeader.get(2), notifs);
//        listDataChild.put(listDataHeader.get(3), notifs);
//        listDataChild.put(listDataHeader.get(4), notifs);
//        listDataChild.put(listDataHeader.get(5), notifs);
//        listDataChild.put(listDataHeader.get(6), notifs);
//        listDataChild.put(listDataHeader.get(7), notifs);
//        listDataChild.put(listDataHeader.get(8), notifs);
//        listDataChild.put(listDataHeader.get(9), notifs);
//        listDataChild.put(listDataHeader.get(10), notifs);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v,
                                int groupPosition, int childPosition, long id) {
      /*  Toast.makeText(
                getApplicationContext(),
                listDataHeader.get(groupPosition)
                        + " : "
                        + listDataChild.get(
                        listDataHeader.get(groupPosition)).get(
                        childPosition), Toast.LENGTH_SHORT)
                .show();*/


        /*Toast.makeText(
                getApplicationContext(),"-"+childPosition, Toast.LENGTH_SHORT)
                .show();*/

        if (childPosition == 0) {


        } else if (childPosition == 1) {

        } else if (childPosition == 2) {

        } else if (childPosition == 3) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        try {
            Utils.mkFolder(this, Utils.FOLDER_BIZGALLERY);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (requestCode == 7 && resultCode == RESULT_OK) {

            try {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null) {
                    try {
                        ImageCaptureBitmap = bitmap;
                        ScanProcessBitmap(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        if (requestCode == PICK_IMAGE_ID && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = mContext.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


//            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

            Bitmap reducedSizeBitmap = getBitmap(picturePath);
            if (reducedSizeBitmap != null) {
//                ImgPhoto.setImageBitmap(reducedSizeBitmap);
//                Button uploadImageButton = (Button) findViewById(R.id.uploadUserImageButton);
//                uploadImageButton.setVisibility(View.VISIBLE);

                try {
                    FullImagePath = picturePath;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ImageCaptureBitmap = reducedSizeBitmap;

               /* try {
                    ScanProcessBitmap(reducedSizeBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                linearFooter.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);
                Surfacefrmlayout.setVisibility(View.GONE);

                // ScanProcessBitmap(ImageCaptureBitmap);

                AsyncTaskExample asyncTask = new AsyncTaskExample();
                asyncTask.execute("");


            } else {
                Toast.makeText(this, "Error while capturing Image", Toast.LENGTH_LONG).show();
            }


//            manualimgcreate.setImageBitmap(BitmapFactory.decodeFile(picturePath));

//            Toast.makeText(mContext, "get image path" + picturePath, Toast.LENGTH_LONG).show();
        }


        String result = null;
        try {
            result = data.getStringExtra("result");

//            Toast.makeText(mContext, "Response--" + result, Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (requestCode == 1) {
            if (resultCode == AppCompatActivity.RESULT_OK) {

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Login Successfull  !")
                        .setMessage(result)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
//                                Intent mainIntent = new Intent(MainActivity.this, MainActivity.class);
//                                startActivity(mainIntent);
                            }
                        })

                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }
            if (resultCode == AppCompatActivity.RESULT_CANCELED) {

            }
        }
    }//onActivityResult


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(MainActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void ScanProcessBitmap(Bitmap bitmap) {

//        cameraView.setVisibility(View.VISIBLE);
//        Surfacefrmlayout.setVisibility(View.VISIBLE);
//        textRecognizer = new TextRecognizer.Builder(this).build();


        if (!mastertextRecognizer.isOperational()) {
            Log.w(TAG, "Text recognizer is not operational!");

        } else {


            Frame frame = new Frame.Builder().setBitmap(bitmap).build();

//            SparseArray<TextBlock> item = textRecognizer.detect(frame);

            profile_data.clear();
            SparseArray<TextBlock> items = mastertextRecognizer.detect(frame);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < items.size(); i++) {
                sb.append(items.valueAt(i).getValue()).append("\n");
                profile_data.add(items.valueAt(i).getValue());
            }

//            cameraView.setVisibility(View.GONE);
//            Surfacefrmlayout.setVisibility(View.GONE);

            Bundle bundle = new Bundle();
            bundle.putStringArrayList(PROFILE_DATA_KEY, (ArrayList<String>) profile_data);
//                bundle.setArguments(bundle);
            ProfileCreatorFragment ProfileListAct = new ProfileCreatorFragment();
//            Settings ProfileListAct = new Settings();
            ProfileListAct.setArguments(bundle);
            Utils.SetFragment(ProfileListAct, mContext, "ProfileCreator");


/*
            textRecognizer.receiveFrame(frame);
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {

                @Override
                public void release() {

                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {

                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0) {
                        textView.post(new Runnable() {
                            @Override
                            public void run() {
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < items.size(); i++) {
                                    sb.append(items.valueAt(i).getValue()).append("\n");
                                }
                                String capturedString = sb.toString();
                                textView.setText(capturedString);
                                if (StringUtils.isNotBlank(capturedString)) {
                                    profile_data.add(capturedString);
                                }
                            }
                        });
                    }
                }
            });
*/

           /* cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setRequestedFps(2.0f)
                    .setAutoFocusEnabled(true)
                    .build();*/
          /*  cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_PERMISSION_ID);
                        return;
                    }
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                    cameraSource.stop();
                }

            });*/


        }


      /*  handler.postDelayed(new Runnable() {
            @Override
            public void run() {

//                Intent intent = new Intent(CameraReaderActivity.this, ProfileCreatorActivity.class);
//                intent.putStringArrayListExtra(PROFILE_DATA_KEY, (ArrayList<String>) profile_data);
                //  startActivity(intent);

//                cameraView.setVisibility(View.GONE);
//                Surfacefrmlayout.setVisibility(View.GONE);


                try {
//                    cameraSource.release();
//                    cameraSource = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Bundle bundle = new Bundle();
                bundle.putStringArrayList(PROFILE_DATA_KEY, (ArrayList<String>) profile_data);
//                bundle.setArguments(bundle);
               // ProfileCreatorFragment ProfileListAct = new ProfileCreatorFragment();
                Settings ProfileListAct = new Settings();
                ProfileListAct.setArguments(bundle);
                Utils.SetFragment(ProfileListAct, mContext, "ProfileCreator");


//                finish();
            }
        }, CAMERA_TIME);*/


    }


    private Bitmap getBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }


    FileOutputStream fo;
    private String FLASH_MODE;
    private int QUALITY_MODE = 0;

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {


                try {

                    if (ImageCaptureBitmap != null) {
                        ImageCaptureBitmap.recycle();
                        ImageCaptureBitmap = null;
                    }

                    System.gc();
                    ImageCaptureBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                linearFooter.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);
                Surfacefrmlayout.setVisibility(View.GONE);

                // ScanProcessBitmap(ImageCaptureBitmap);

                AsyncTaskExample asyncTask = new AsyncTaskExample();
                asyncTask.execute("");


            }
        };
        return picture;
    }


    private void takeImage() {
        try {
            //openCamera(CameraInfo.CAMERA_FACING_BACK);
            //releaseCameraSource();
            //releaseCamera();
            //openCamera(CameraInfo.CAMERA_FACING_BACK);
            //setUpCamera(camera);
            //Thread.sleep(1000);
            /*cameraSource.takePicture(null, new CameraSource.PictureCallback() {

                private File imageFile;
                @Override
                public void onPictureTaken(byte[] bytes) {
                    try {
                        // convert byte array into bitmap
                        Bitmap loadedImage = null;
                        Bitmap rotatedBitmap = null;
                        loadedImage = BitmapFactory.decodeByteArray(bytes, 0,
                                bytes.length);




                        // rotate Image
                        Matrix rotateMatrix = new Matrix();
                        rotateMatrix.postRotate(rotation);
                        rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                                loadedImage.getWidth(), loadedImage.getHeight(),
                                rotateMatrix, false);
                        String state = Environment.getExternalStorageState();
                        File folder = null;
                        if (state.contains(Environment.MEDIA_MOUNTED)) {
                            folder = new File(Environment
                                    .getExternalStorageDirectory() + "/Demo");
                        } else {
                            folder = new File(Environment
                                    .getExternalStorageDirectory() + "/Demo");
                        }

                        boolean success = true;
                        if (!folder.exists()) {
                            success = folder.mkdirs();
                        }
                        if (success) {
                            java.util.Date date = new java.util.Date();
                            imageFile = new File(folder.getAbsolutePath()
                                    + File.separator
                                    //+ new Timestamp(date.getTime()).toString()
                                    + "Image.jpg");

                            imageFile.createNewFile();
                        } else {
                            Toast.makeText(getBaseContext(), "Image Not saved",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                        // save image into gallery
                        rotatedBitmap = resize(rotatedBitmap, 800, 600);
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);

                        FileOutputStream fout = new FileOutputStream(imageFile);
                        fout.write(ostream.toByteArray());
                        fout.close();
                        ContentValues values = new ContentValues();

                        values.put(MediaStore.Images.Media.DATE_TAKEN,
                                System.currentTimeMillis());
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                        values.put(MediaStore.MediaColumns.DATA,
                                imageFile.getAbsolutePath());

                        mContext.getContentResolver().insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        setResult(Activity.RESULT_OK); //add this
//                        finish();


                        // as last needed code


                        FullImagePath = imageFile.getAbsolutePath();

                        ImageCaptureBitmap = rotatedBitmap;
                        try {
                            ScanProcessBitmap(rotatedBitmap);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }




                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });*/

        } catch (Exception ex) {
            //       txTextoCapturado.setText("Error al capturar fotografia!");
        }

    }


    private Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }


    private int cameraId;
    private int rotation;

    private void setUpCamera(Camera c) {

        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        c.setDisplayOrientation(rotation);
        Camera.Parameters params = c.getParameters();

//        showFlashButton(params);

        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        params.setRotation(rotation);
    }


    private boolean openCamera(int id) {
        boolean result = false;
        cameraId = id;
        //releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera);
                camera.setErrorCallback(new Camera.ErrorCallback() {

                    @Override
                    public void onError(int error, Camera camera) {

                    }
                });
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }
        return result;
    }


    private void releaseCamera() {

        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications

        try {
            releaseCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview

                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                mCamera = Camera.open(cameraId);
                mCamera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
            }
        }
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;

            }

        }
        return cameraId;
    }


    private CameraPreview mPreview;
    private LinearLayout cameraPreview;

    private void MainCreateCamera() {

       /* try {
            releaseCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

        Surfacefrmlayout.setVisibility(View.VISIBLE);
        try {
            mCamera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mastertextRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

//
//        cameraPreview = (LinearLayout) findViewById(R.id.cPreview);

//        mCamera =  Camera.open();
//        mCamera.setDisplayOrientation(90);


//        mPreview = new CameraPreview(mContext, mCamera);
//        cameraPreview.addView(mPreview);

//        chooseCamera();


//        mCamera =  Camera.open();
//        mCamera.setDisplayOrientation(90);
//        cameraPreview = (LinearLayout) findViewById(R.id.cPreview);
//        mPreview = new CameraPreview(mContext, mCamera);
//        cameraPreview.addView(mPreview);

    }

    private int findFrontFacingCamera() {

        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;

    }


    ProgressDialog p;

    private class AsyncTaskExample extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            p = new ProgressDialog(mContext);
            p.setMessage("Please wait Scaning Card..");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();


            //card_view_Home.setVisibility(View.GONE);
            linearFooter.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            Surfacefrmlayout.setVisibility(View.GONE);

        }

        @Override
        protected String doInBackground(String... strings) {

            DoInBackground();

            return "";
        }

        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");

            try {
                p.hide();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    TextRecognizer mastertextRecognizer;

    private void DoInBackground() {


        /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (ImageCaptureBitmap != null && QUALITY_MODE == 0)
            ImageCaptureBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        else if (ImageCaptureBitmap != null && QUALITY_MODE != 0)
            ImageCaptureBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY_MODE, bytes);

        File imagesFolder = new File(
                Environment.getExternalStorageDirectory(), Utils.FOLDER_BIZGALLERY);
        if (!imagesFolder.exists())
            imagesFolder.mkdirs(); // <----
        File image = new File(imagesFolder, "BizImg"+System.currentTimeMillis()
                + ".jpg");

        // write the bytes in file

        try {
            FullImagePath = image.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            fo = new FileOutputStream(image);
        } catch (FileNotFoundException e) {
            Log.e("TAG", "FileNotFoundException", e);
            // TODO Auto-generated catch block
        }
        try {
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            Log.e("TAG", "fo.write::PictureTaken", e);
            // TODO Auto-generated catch block
        }

        // remember close de FileOutput
        try {
            fo.close();
            if (Build.VERSION.SDK_INT < 19)
                sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://"
                                + Environment.getExternalStorageDirectory())));
            else {
                MediaScannerConnection
                        .scanFile(
                                getApplicationContext(),
                                new String[] { image.toString() },
                                null,
                                new MediaScannerConnection.OnScanCompletedListener() {
                                    public void onScanCompleted(
                                            String path, Uri uri) {
                                        Log.i("ExternalStorage", "Scanned "
                                                + path + ":");
                                        Log.i("ExternalStorage", "-> uri="
                                                + uri);
                                    }
                                });
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
*/

        //Intent intent = new Intent(MainActivity.this,PictureActivity.class);
        //startActivity(intent);
        ScanProcessBitmap(ImageCaptureBitmap);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }



    private boolean saveGroupSetting(String strSetting) {
        return profileDao.insertGroupSetting(strSetting);
    }



    public String getGroupSetting() {


        String strSetting = null;
        Cursor groupData = profileDao.GroupSetting();

        if (groupData == null) {

//            Utils.displayErrorDialog(mContext);

        } else {

            for (groupData.moveToFirst(); !groupData.isAfterLast(); groupData.moveToNext()) {
                try {
//                    strId = groupData.getString(0);
                    strSetting = groupData.getString(1);

                } catch (Exception e) {
                    e.printStackTrace();
                    strSetting= null;
                }
            }

        }

        return  strSetting;
    }
}
