package com.bizcards.act.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bizcards.act.MasterHomeFrgament;
import com.bizcards.act.R;
import com.bizcards.act.Utils;
import com.bizcards.act.interfaceAll.GetNavId;
//import com.bizcards.act.ui.home.HomeFragment;

import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 11/12/15.
 */
public class NavAdapter extends BaseExpandableListAdapter {


    int[]   sideimage = {R.drawable.side1,R.drawable.side2,R.drawable.side3,R.drawable.side4,R.drawable.side5,R.drawable.side5,R.drawable.side5,R.drawable.side5};


    public LayoutInflater minflater;
    public Activity activity;
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    GetNavId   interGetNavId;

    public NavAdapter(Context context, List<String> listDataHeader,
                      HashMap<String, List<String>> listChildData,GetNavId  objGetNavId) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.interGetNavId = objGetNavId;
    }

    public void setInflater(LayoutInflater mInflater, Activity act) {
        this.minflater = mInflater;
        activity = act;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {


        int msize = 0;
        try{
             msize = this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
            return  msize;
        }catch (Exception e){
            e.printStackTrace();
        }

        return msize;

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);


        lblListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                interGetNavId.getNevId(groupPosition);

                //HomeFragment ProfileListAct = new HomeFragment();
                //Utils.SetFragment(ProfileListAct, _context, "HomeFragment");

              /*  MasterHomeFrgament master = new MasterHomeFrgament();
                Utils.SetFragment(master, _context, "HomeFragment");*/

            }
        });
        ImageView imgArrow = (ImageView) convertView.findViewById(R.id.imgside);

        imgArrow.setImageResource(sideimage[groupPosition]);

       /* if(groupPosition==1){
            imgArrow.setVisibility(View.VISIBLE);
            imgArrow.setRotation(270f);
        }else{
            imgArrow.setVisibility(View.GONE);
        }

        if (isExpanded) {

            imgArrow.setRotation(0f);

//            groupHolder.img.setImageResource(R.drawable.group_down);
        } else {

            imgArrow.setRotation(270f);

//            groupHolder.img.setImageResource(R.drawable.group_up);
        }
*/
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



    public   void  SetGetNavId(GetNavId   objGetNavId){
        this.interGetNavId = objGetNavId;
    }

}