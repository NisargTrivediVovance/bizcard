package com.bizcards.act.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bizcards.act.GroupAdapter;
import com.bizcards.act.GroupViewHolder;
import com.bizcards.act.MasterHomeFrgament;
import com.bizcards.act.Profile;
import com.bizcards.act.ProfileViewFragment;
import com.bizcards.act.R;
import com.bizcards.act.Utils;
import com.bizcards.act.interfaceAll.GetDeleteId;

import java.util.ArrayList;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

//import static com.bizcards.act.MainActivity.card_view_Home;
//import static com.bizcards.act.MainActivity.imgMenu;
//import static com.bizcards.act.MainActivity.linearProfile_details;

public class GroupDetailsAdapter extends RecyclerView.Adapter<GroupViewHolder> {

    private static final String TAG = "SportAdapter";
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;
    private GroupAdapter.Callback mCallback;

    GetDeleteId objGetDeleteId;
    Context context;
    ArrayList<Profile> newgrouplist;

    public void SetGetDeleteId(GetDeleteId  mObjGetDeleteId){
        this.objGetDeleteId = mObjGetDeleteId;
    }

    public GroupDetailsAdapter (Context mcontext,ArrayList<Profile> grouplist,GetDeleteId  mobjGetDeleteId){
        context = mcontext;
        newgrouplist = grouplist;
        this.objGetDeleteId = mobjGetDeleteId;
    }

    // private List<Sport> mSportList;
  /*  public SportAdapter(List<Sport> sportList) {
        mSportList = sportList;
    }*/
    public void setCallback(GroupAdapter.Callback callback) {
        mCallback = callback;
    }
    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new GroupDetailsAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row_favorite, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new GroupDetailsAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_empty_view, parent, false));
        }
    }
    @Override
    public int getItemViewType(int position) {
        if (newgrouplist != null && newgrouplist.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }
    @Override
    public int getItemCount() {
        if (newgrouplist != null && newgrouplist.size() > 0) {
            return newgrouplist.size();
        } else {
            return 1;
        }
    }
    /* public void addItems(List<Sport> sportList) {
         mSportList.addAll(sportList);
         notifyDataSetChanged();
     }*/
    public interface Callback {
        void onEmptyViewRetryClick();
    }
    public class ViewHolder extends GroupViewHolder {
        // @BindView(R.id.thumbnail)
        /*ImageView coverImageView;
        @BindView(R.id.title)
        TextView titleTextView;
        @BindView(R.id.newsTitle)
        TextView newsTextView;
        @BindView(R.id.newsInfo)
        TextView infoTextView;*/
//        CheckBox chkbox_group;
        TextView txtName,txtjobtitle,txtCompany;
        RelativeLayout relative_expand;

        ImageView imgicon,imgMenu;

        public ViewHolder(View itemView) {
            super(itemView);


            imgicon = (ImageView) itemView.findViewById(R.id.imgicon);
            // ButterKnife.bind(this, itemView);
//            chkbox_group = (CheckBox)itemView.findViewById(R.id.chkbox_group);
            txtName = (TextView)itemView.findViewById(R.id.txtName);
            txtjobtitle = (TextView) itemView.findViewById(R.id.txtjobtitle);
            txtCompany = (TextView) itemView.findViewById(R.id.txtCompany);
            imgMenu = (ImageView)  itemView.findViewById(R.id.imgMenu);

            relative_expand = (RelativeLayout) itemView.findViewById(R.id.relative_expand);

        }
        protected void clear() {
            //coverImageView.setImageDrawable(null);
            txtName.setText("");
            //infoTextView.setText("");
        }
        public void onBind(int position) {
            super.onBind(position);


            Profile getFavorite = newgrouplist.get(position);


            try {
//                byte[] image = getFavorite.getImageBlob();
//                Bitmap bmp= BitmapFactory.decodeByteArray(image, 0 , image.length);
//                imgicon.setImageBitmap(bmp);

                Bitmap  bmp = Utils.getBitmap(context,getFavorite.getImagepath());
                imgicon.setImageBitmap(bmp);


            } catch (Exception e) {
                e.printStackTrace();
            }


            txtName.setText(getFavorite.getName());
            txtjobtitle.setText(getFavorite.getJobTitle());
            txtCompany.setText(getFavorite.getIsFavorite());

            imgMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PopupMenu menu = new PopupMenu(context, view);
//                menu.getMenu().add(1, 1, 1, "Share");
                    menu.getMenu().add(2, 2, 2, "Delete");
                    menu.show();

                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case 1:
//                                ShareImageIntent();
                                    return true;
                                case 2:

//                                    int mId = Integer.parseInt(getFavorite.getId());

                                    Profile getFavorite = newgrouplist.get(position);
                                    objGetDeleteId.getMDeleteId(getFavorite);
//                                    objGetDeleteId.getMDeleteId(getFavorite);
//                                Toast.makeText(_context, "Delete..InProcess", Toast.LENGTH_SHORT).show();


                                    return true;

                                default:

                                    return true;
                            }

                        }
                    });


                }
            });

            relative_expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {
                        MasterHomeFrgament.ProfileId  = Integer.valueOf(getFavorite.getId());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//                imgMenu.setVisibility(View.GONE);
//                    card_view_Home.setVisibility(View.GONE);
                    //linearProfile_details.setVisibility(View.VISIBLE);
//                    imgMenu.setImageResource(R.drawable.back_icon);
//                ((AppCompatActivity)(context)).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                ((AppCompatActivity)(context)).getSupportActionBar().setDisplayShowHomeEnabled(true);

                    ProfileViewFragment nearFragment = new ProfileViewFragment();
                    Utils.SetFragment(nearFragment, context, "Explorer");

                }
            });




           /* final Sport mSport = mSportList.get(position);
            if (mSport.getImageUrl() != null) {
                Glide.with(itemView.getContext())
                        .load(mSport.getImageUrl())
                        .into(coverImageView);
            }*/
           /* if (mSport.getTitle() != null) {
                titleTextView.setText(mSport.getTitle());
            }
            if (mSport.getSubTitle() != null) {
                newsTextView.setText(mSport.getSubTitle());
            }
            if (mSport.getInfo() != null) {
                infoTextView.setText(mSport.getInfo());
            }*/
           /* itemView.setOnClickListener(v -> {
                if (mSport.getImageUrl() != null) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse(mSport.getImageUrl()));
                        itemView.getContext().startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG, "onClick: Image url is not correct");
                    }
                }
            });*/
        }
    }
    public class EmptyViewHolder extends GroupViewHolder {
        /* @BindView(R.id.tv_message)
         TextView messageTextView;
         @BindView(R.id.buttonRetry)
         TextView buttonRetry;*/
        EmptyViewHolder(View itemView) {
            super(itemView);
            //ButterKnife.bind(this, itemView);
            //buttonRetry.setOnClickListener(v -> mCallback.onEmptyViewRetryClick());
        }
        @Override
        protected void clear() {
        }
    }
}


