package com.bizcards.act.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bizcards.act.MasterHomeFrgament;
import com.bizcards.act.Profile;
import com.bizcards.act.ProfileDao;
import com.bizcards.act.ProfileViewFragment;
import com.bizcards.act.R;
import com.bizcards.act.Utils;
import com.bizcards.act.interfaceAll.GetDeleteId;

import java.util.HashMap;
import java.util.List;

//import static com.bizcards.act.MainActivity.card_view_Home;
//import static com.bizcards.act.MainActivity.imgMenu;
//import static com.bizcards.act.MainActivity.linearProfile_details;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;

import static com.bizcards.act.MainActivity.masterFragmentManager1;
import static com.bizcards.act.ProfileArrayAdapter.DELIMITER;

public class ExpandableList_Home extends BaseExpandableListAdapter {


    int[] sideimage = {R.drawable.side1, R.drawable.side2, R.drawable.side3, R.drawable.side4, R.drawable.side5, R.drawable.side5, R.drawable.side5, R.drawable.side5};


    public LayoutInflater minflater;
    public Activity activity;
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Profile>> _listDataChild;

    GetDeleteId objGetDeleteId;

    public ExpandableList_Home(Context context, List<String> listDataHeader,
                               HashMap<String, List<Profile>> listChildData,GetDeleteId  mobjGetDeleteId) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.objGetDeleteId = mobjGetDeleteId;
    }

    public void SetGetDeleteId(GetDeleteId  mObjGetDeleteId){
        this.objGetDeleteId = mObjGetDeleteId;
    }

    public void setInflater(LayoutInflater mInflater, Activity act) {
        this.minflater = mInflater;
        activity = act;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        // final String childText = (String) getChild(groupPosition, childPosition);

        final Profile childText = (Profile) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_expandble, null);
        }

      /*  final String[] text = (childText.split(DELIMITER));
//        HomeFragment.ProfileId  = Integer.valueOf(text[1]);*/

//        final String[] text = (childText.split(DELIMITER));


        ImageView img_menu = (ImageView) convertView.findViewById(R.id.img_menu);

        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu menu = new PopupMenu(_context, view);
//                menu.getMenu().add(1, 1, 1, "Share");
                menu.getMenu().add(2, 2, 2, "Delete");
                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 1:
//                                ShareImageIntent();
                                return true;
                            case 2:

                                int mId = Integer.parseInt(childText.getId());

                                objGetDeleteId.getMDeleteId(childText);
//                                Toast.makeText(_context, "Delete..InProcess", Toast.LENGTH_SHORT).show();


                                return true;

                            default:

                                return true;
                        }

                    }
                });


            }
        });


        ImageView imgicon = (ImageView) convertView.findViewById(R.id.imgicon);

        try {
//            byte[] image = childText.getImageBlob();
            //Bitmap bmp= BitmapFactory.decodeByteArray(image, 0 , image.length);
            Bitmap bmp = null;
            bmp = Utils.getBitmap(_context, childText.getImagepath());
            imgicon.setImageBitmap(bmp);
        } catch (Exception e) {
            e.printStackTrace();
        }


        RelativeLayout relative_expand = (RelativeLayout) convertView.findViewById(R.id.relative_expand);
        relative_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    MasterHomeFrgament.ProfileId = Integer.valueOf(childText.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                imgMenu.setVisibility(View.GONE);
                //card_view_Home.setVisibility(View.GONE);
                //linearProfile_details.setVisibility(View.VISIBLE);
//                imgMenu.setImageResource(R.drawable.back_icon);
//                ((AppCompatActivity)(context)).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                ((AppCompatActivity)(context)).getSupportActionBar().setDisplayShowHomeEnabled(true);

                ProfileViewFragment nearFragment = new ProfileViewFragment();
                Utils.SetFragment(nearFragment, _context, "Explorer");

            }
        });


        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        TextView txtjobtitle = (TextView) convertView.findViewById(R.id.txtjobtitle);
        TextView txtCompany = (TextView) convertView.findViewById(R.id.txtCompany);

        try {
            txtName.setText(childText.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            txtjobtitle.setText(childText.getJobTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            txtCompany.setText(childText.getCompany());
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        */

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        int i = 0;

        try {

            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        //list_group
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // convertView = infalInflater.inflate(R.layout.row_expandble, null);
            convertView = infalInflater.inflate(R.layout.list_group_expand, null);
        }


        LayoutInflater infalInflater;

        if (_listDataHeader.size() > 0) {
            infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // convertView = infalInflater.inflate(R.layout.row_expandble, null);
            convertView = infalInflater.inflate(R.layout.list_group_expand, null);

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);


            ImageView imgArrow = (ImageView) convertView.findViewById(R.id.imgside);
            imgArrow.setVisibility(View.GONE);

        } else {
            infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // convertView = infalInflater.inflate(R.layout.row_expandble, null);
            convertView = infalInflater.inflate(R.layout.item_empty_view, null);

        }


        //        imgArrow.setImageResource(sideimage[groupPosition]);

       /* if(groupPosition==1){
            imgArrow.setVisibility(View.VISIBLE);
            imgArrow.setRotation(270f);
        }else{
            imgArrow.setVisibility(View.GONE);
        }

        if (isExpanded) {

            imgArrow.setRotation(0f);

//            groupHolder.img.setImageResource(R.drawable.group_down);
        } else {

            imgArrow.setRotation(270f);

//            groupHolder.img.setImageResource(R.drawable.group_up);
        }
*/
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}