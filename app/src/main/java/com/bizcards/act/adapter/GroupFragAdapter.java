package com.bizcards.act.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bizcards.act.GroupAdapter;
import com.bizcards.act.GroupViewHolder;
import com.bizcards.act.Group_Details_Fragment;
import com.bizcards.act.Profile;
import com.bizcards.act.ProfileCreatorFragment;
import com.bizcards.act.R;
import com.bizcards.act.Utils;
import com.bizcards.act.pojo.GroupCards;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class GroupFragAdapter extends RecyclerView.Adapter<GroupViewHolder> {

    private static final String TAG = "SportAdapter";
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;
    private GroupAdapter.Callback mCallback;

    Context context;
    ArrayList<GroupCards> newgrouplist;

    public static String StrGroupName;

    public static  ArrayList<Profile>  mGProfile;
    public GroupFragAdapter (Context mcontext, ArrayList<GroupCards> grouplist){
        context = mcontext;
        newgrouplist = grouplist;
    }

    // private List<Sport> mSportList;
  /*  public SportAdapter(List<Sport> sportList) {
        mSportList = sportList;
    }*/
    public void setCallback(GroupAdapter.Callback callback) {
        mCallback = callback;
    }
    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new GroupFragAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row_group_fragm, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new GroupFragAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_empty_view, parent, false));
        }
    }
    @Override
    public int getItemViewType(int position) {
        if (newgrouplist != null && newgrouplist.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }
    @Override
    public int getItemCount() {
        if (newgrouplist != null && newgrouplist.size() > 0) {
            return newgrouplist.size();
        } else {
            return 1;
        }
    }
    /* public void addItems(List<Sport> sportList) {
         mSportList.addAll(sportList);
         notifyDataSetChanged();
     }*/
    public interface Callback {
        void onEmptyViewRetryClick();
    }
    public class ViewHolder extends GroupViewHolder {
        // @BindView(R.id.thumbnail)
        /*ImageView coverImageView;
        @BindView(R.id.title)
        TextView titleTextView;
        @BindView(R.id.newsTitle)
        TextView newsTextView;
        @BindView(R.id.newsInfo)
        TextView infoTextView;*/
//        CheckBox chkbox_group;
        TextView txtName,txtCount;

        ImageView grouptpe_icon;
        RelativeLayout relative_group;

        public ViewHolder(View itemView) {
            super(itemView);
            // ButterKnife.bind(this, itemView);
//            chkbox_group = (CheckBox)itemView.findViewById(R.id.chkbox_group);
            txtName = (TextView)itemView.findViewById(R.id.txtName);
            txtCount= (TextView) itemView.findViewById(R.id.txtCount);
            relative_group  = (RelativeLayout) itemView.findViewById(R.id.relative_group);

            grouptpe_icon = (ImageView)itemView.findViewById(R.id.grouptpe_icon);

        }
        protected void clear() {
            //coverImageView.setImageDrawable(null);
            txtName.setText("");
            //infoTextView.setText("");
        }
        public void onBind(int position) {
            super.onBind(position);

            if(position==0){
                grouptpe_icon.setImageResource(R.drawable.business_group_icon);
            }else if( position == 1){
                grouptpe_icon.setImageResource(R.drawable.family_group_icon);
            }else if( position == 2){
                grouptpe_icon.setImageResource(R.drawable.vip_group_icon);
            }else if( position == 3){
                grouptpe_icon.setImageResource(R.drawable.collages_group_icon);
            }else if( position == 4){
                grouptpe_icon.setImageResource(R.drawable.customer_group_icon);
            }else{
                grouptpe_icon.setImageResource(R.drawable.groupicon);
            }



            GroupCards getgrp_name = null;
            try {
                getgrp_name = newgrouplist.get(position);
                txtName.setText(getgrp_name.getName());



            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                mGProfile = getgrp_name.getmProfiles();
                txtCount.setText(""+mGProfile.size());
            } catch (Exception e) {
                e.printStackTrace();
            }

            txtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                        try {
                            GroupCards getgrp_name = null;
                            getgrp_name = newgrouplist.get(position);
                            mGProfile = getgrp_name.getmProfiles();
                            StrGroupName = getgrp_name.getName();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    Group_Details_Fragment groupDetails = new Group_Details_Fragment();
//                    groupDetails.setArguments(bundle);
                    Utils.SetFragment(groupDetails, context, "GroupDetails");
                }
            });


            relative_group.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        GroupCards getgrp_name = null;
                        getgrp_name = newgrouplist.get(position);
                        mGProfile = getgrp_name.getmProfiles();
                        StrGroupName = getgrp_name.getName();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                    Group_Details_Fragment groupDetails = new Group_Details_Fragment();
//                    groupDetails.setArguments(bundle);
                    Utils.SetFragment(groupDetails, context, "GroupDetails");
                }
            });


           /* final Sport mSport = mSportList.get(position);
            if (mSport.getImageUrl() != null) {
                Glide.with(itemView.getContext())
                        .load(mSport.getImageUrl())
                        .into(coverImageView);
            }*/
           /* if (mSport.getTitle() != null) {
                titleTextView.setText(mSport.getTitle());
            }
            if (mSport.getSubTitle() != null) {
                newsTextView.setText(mSport.getSubTitle());
            }
            if (mSport.getInfo() != null) {
                infoTextView.setText(mSport.getInfo());
            }*/
           /* itemView.setOnClickListener(v -> {
                if (mSport.getImageUrl() != null) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse(mSport.getImageUrl()));
                        itemView.getContext().startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG, "onClick: Image url is not correct");
                    }
                }
            });*/
        }
    }
    public class EmptyViewHolder extends GroupViewHolder {
        /* @BindView(R.id.tv_message)
         TextView messageTextView;
         @BindView(R.id.buttonRetry)
         TextView buttonRetry;*/
        EmptyViewHolder(View itemView) {
            super(itemView);
            //ButterKnife.bind(this, itemView);
            //buttonRetry.setOnClickListener(v -> mCallback.onEmptyViewRetryClick());
        }
        @Override
        protected void clear() {
        }
    }
}
