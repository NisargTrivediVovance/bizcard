package com.bizcards.act;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bizcards.act.interfaceAll.GetGroupId;
import com.bizcards.act.pojo.GroupCards;
//import com.bizcards.act.ui.home.HomeFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import static com.bizcards.act.MainActivity.card_view_Home;
import static android.app.Activity.RESULT_OK;
import static com.bizcards.act.MainActivity.linearFooter;
import static com.bizcards.act.MainActivity.masterFragmentManager1;
//import static com.bizcards.act.MainActivity.linearProfile_details;

public class Manual_Profile_Creator_Fragment extends Fragment {

    Context mContext;
    ImageView manualimgcreate;

    public static final int PICK_IMAGE_ID = 234; // the number doesn't matter
    //CardView  card_view8;


    EditText input_name, input_jobtitle, input_company, input_Number, input_email, input_website,input_Adress;
    String str_name, str_jobtitle, str_company, str_number, str_email, str_web,str_Address;

    Button save_button;
    private ProfileDao profileDao;


    ArrayList<String> selectedGroupId;


    Button btn_addnew_group;
    RelativeLayout rel_lable;
    RecyclerView list_of_new_group;
    GroupAdapter grpadpter;
    LinearLayoutManager mLayoutManager;
    Button imgbtn_plus;
    Boolean boolean_group_hideshow = false;

    Uri outputFileUri;
    String selectedImagePath = "";
    byte[]  FullImageData;


    ImageView  imgback;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.manual_create_profile, container, false);


        profileDao = ProfileDao.getInstance(mContext);

        mContext = Manual_Profile_Creator_Fragment.this.getActivity();


        imgback = (ImageView) root.findViewById(R.id.imgback);

        save_button = (Button) root.findViewById(R.id.save_button);

        input_name = (EditText) root.findViewById(R.id.input_name);
        input_jobtitle = (EditText) root.findViewById(R.id.input_jobtitle);
        input_company = (EditText) root.findViewById(R.id.input_company);
        input_Number = (EditText) root.findViewById(R.id.input_Number);
        input_email = (EditText) root.findViewById(R.id.input_email);
        input_website = (EditText) root.findViewById(R.id.input_website);
        input_Adress = (EditText) root.findViewById(R.id.input_Adress);

        imgbtn_plus = (Button)root.findViewById(R.id.imgbtn_plus);
        btn_addnew_group = (Button)root.findViewById(R.id.btn_addnew_group);
        rel_lable = (RelativeLayout)root.findViewById(R.id.rel_lable);
        list_of_new_group = (RecyclerView) root.findViewById(R.id.list_of_new_group);

        manualimgcreate = (ImageView)root.findViewById(R.id.manualimgcreate);



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    if (masterFragmentManager1.getBackStackEntryCount() > 0) {
                        masterFragmentManager1.popBackStack();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


        btn_addnew_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CreateGroup();
            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                str_name = input_name.getText().toString();
                str_jobtitle = input_jobtitle.getText().toString();
                str_company = input_company.getText().toString();
                str_number = input_Number.getText().toString();
                str_email = input_email.getText().toString();
                str_web = input_website.getText().toString();
                str_Address = input_Adress.getText().toString();


                validateAndCreateProfile();

            }
        });

        manualimgcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, PICK_IMAGE_ID);

            }
        });
        setUp();


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

//        linearProfile_details.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:


                if (requestCode == PICK_IMAGE_ID && resultCode == RESULT_OK && null != data) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Cursor cursor = mContext.getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();



                    selectedImagePath = picturePath;


                    Bitmap reducedSizeBitmap = getBitmap(picturePath);
                    if(reducedSizeBitmap != null){


                        manualimgcreate.setImageBitmap(reducedSizeBitmap);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                        FullImageData = stream.toByteArray();
                        //reducedSizeBitmap.recycle();

                        //manualimgcreate.setImageBitmap(BitmapFactory.decodeFile(picturePath));

                    }else{
                        Toast.makeText(mContext,"Error while capturing Image",Toast.LENGTH_LONG).show();
                    }



//                     Toast.makeText(mContext, "get image path"+picturePath, Toast.LENGTH_LONG).show();
                }


                // TODO use bitmap
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }
    private boolean saveProfile(Profile profile) {
        return profileDao.insert(profile);
    }



    String groupId = "";

    ArrayList<String> grouNameMatching;

    private void validateAndCreateProfile(){


        for(int m=0;m<selectedGroupId.size();m++){
            groupId = selectedGroupId.get(m);
        }

//        Toast.makeText(mContext,"GId--"+groupId,Toast.LENGTH_LONG).show();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String strcurrent_Date = sdf.format(new Date());
//        Log.d(TAG, "Date: " + str);
//        String strcurrent_Date = "2020/06/22";
//        str_name, str_jobtitle, str_company, str_number, str_email, str_web
        //input_address,input_website
        Profile profile = new Profile("mid",
                str_name,
                str_jobtitle,
                str_company,
                str_number,
                str_email,str_web,str_Address,strcurrent_Date,"false",groupId,FullImageData,selectedImagePath
        );

        if (profile.isValid()){

            if (saveProfile(profile)) {

                showSaveSuccessDialog();

            } else {

                Utils.displayErrorDialog(mContext);
            }
        } else {
            alertInvalidProfile();
        }
    }


    //===================================================Manually--------------


    private void setUp() {



        try {
            getGroupList();
        } catch (Exception e) {
            e.printStackTrace();
        }

      /*  grpdata = new ArrayList<>();
        grpdata.add("Business");
        grpdata.add("Family");
        grpdata.add("Vip");
        grpdata.add("Colleagues");
        grpdata.add("Customer");*/


        if (!grouNameMatching.contains("Business")) {

            try {

                ArrayList<Profile>  BProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Business", BProfiles);
                saveGroup(groupCard);
                //validateAndCreateGroup("Business");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!grouNameMatching.contains("Family")) {

            try {

                ArrayList<Profile>  FProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Family", FProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Family");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Vip")) {

            try {

                ArrayList<Profile>  VProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Vip", VProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Vip");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Colleagues")) {

            try {

                ArrayList<Profile>  CProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Colleagues", CProfiles);
                saveGroup(groupCard);
                //validateAndCreateGroup("Colleagues");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Customer")) {

            try {

                ArrayList<Profile>  CUSProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Customer", CUSProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Customer");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {
            getGroupList();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    ArrayList<GroupCards>   groupList;

    public void getGroupList() {


        selectedGroupId = new ArrayList<>();
        groupList = new ArrayList<GroupCards>();
        grouNameMatching = new ArrayList<>();

        Cursor groupData = profileDao.loadDataForGroup();

        if (groupData == null) {

            Utils.displayErrorDialog(mContext);

        } else {

            for (groupData.moveToFirst(); !groupData.isAfterLast(); groupData.moveToNext()) {

                try {


                   /* result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                            result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Date))
                            */

                    // Profile profile = new Profile("" + profileData.getString(0), "" + profileData.getString(1), "" + profileData.getString(2), "" + profileData.getString(4), "" + profileData.getString(5), "" + profileData.getString(3), "" + profileData.getString(6), "" + profileData.getString(7),"" + profileData.getString(8));

                    //GroupCards profile = new GroupCards("" + groupData.getString(0), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_NAME)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_COMPANY)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_EMAIL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_WEB)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Address)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Date)));

                    //Here we get Actule Dataas
                    //ArrayList<Profile>  getLiveProfils = profileDao.loadGroupCount( groupData.getString(0));

                    ArrayList<Profile>  getLiveProfils = new ArrayList<>();
                    GroupCards group = new GroupCards("" + groupData.getString(0), "" + groupData.getString(1), getLiveProfils);

                    groupList.add(group);
                    String groupname = groupData.getString(1);

                    //grpdata.add(groupname);
                    grouNameMatching.add(groupname);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            Log.d(ProfileListActivity.class.getName(), "Found " + profileItems.size() + " profiles in list");

            //Setup Listview.

            mLayoutManager = new LinearLayoutManager(mContext);
            mLayoutManager.setOrientation(RecyclerView.VERTICAL);
            list_of_new_group.setLayoutManager(mLayoutManager);
            list_of_new_group.setItemAnimator(new DefaultItemAnimator());

            grpadpter = new GroupAdapter(mContext, groupList, new GetGroupId() {
                @Override
                public void getGId(String gId,boolean isChecked) {


                    if(isChecked){

                        if(!selectedGroupId.contains(gId)){
                            selectedGroupId.add(gId);
                        }
//                        Toast.makeText(mContext,""+gId,Toast.LENGTH_LONG).show();

                    }else{

                        if(selectedGroupId.contains(gId)){
                            selectedGroupId.remove(gId);
                        }

//                        Toast.makeText(mContext,""+gId,Toast.LENGTH_LONG).show();
                    }


                }
            });
            list_of_new_group.setAdapter(grpadpter);

        }

    }


    private void CreateGroup(){

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Group");
        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);
        // add a button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // send data from the AlertDialog to the Activity
                EditText editText = customLayout.findViewById(R.id.editText);
//                sendDialogDataToActivity(editText.getText().toString());
                String groupName = editText.getText().toString();
                try {
                    validateAndCreateGroup(groupName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean saveGroup(GroupCards objgroup) {
        return profileDao.insertGroup(objgroup);
    }

    private void validateAndCreateGroup(String groupName){


        String gId="";
        /*for(int m=0;m<selectedGroupId.size();m++){
            gId += selectedGroupId.get(m)+",";
        }*/

//        Toast.makeText(m)
        ArrayList<Profile>  getLiveProfils = new ArrayList<>();
        GroupCards groupCard = new GroupCards("gid",groupName,getLiveProfils );
        if (groupCard.isValid()){
            if (saveGroup(groupCard)) {

                try {
                    getGroupList();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                showGroupSaveSuccessDialog();

            } else {
                Utils.displayErrorDialog(mContext);
            }
        } else {
            alertInvalidProfile();
        }
    }
    private void showSaveSuccessDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.dialog_success);
        builder.setMessage(R.string.profile_creator_save_success_message);
        builder.setCancelable(false); //Don't let them touch out!
        builder.setNegativeButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //card_view_Home.setVisibility(View.VISIBLE);
                        linearFooter.setVisibility(View.VISIBLE);


                        try {
                            dialogInterface.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        MasterHomeFrgament ProfileListAct = new MasterHomeFrgament();
                        Utils.SetFragment(ProfileListAct,mContext , "HomeFragments");

                    }
                });

       /* builder.setPositiveButton(R.string.profile_creator_save_success_scan_another,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      *//*  Intent intent = new Intent(ProfileCreatorActivity.this,
                                CameraReaderActivity.class);
                        startActivity(intent);
                        finish();*//*
                    }
                });
        */

        builder.create().show();
    }
    private void alertInvalidProfile() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.profile_creator_save_invalid_title);
        builder.setMessage(R.string.profile_creator_save_invalid_message);
        builder.setPositiveButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        builder.create().show();
    }
    private void showGroupSaveSuccessDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.dialog_success);
        builder.setMessage(R.string.profile_creator_save_success_message);
        builder.setCancelable(false); //Don't let them touch out!
        builder.setNegativeButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

//                        card_view_Home.setVisibility(View.VISIBLE);
//                        linearFooter.setVisibility(View.VISIBLE);


                        try {
                            getGroupList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            dialogInterface.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        HomeFragment ProfileListAct = new HomeFragment();
//                        Utils.SetFragment(ProfileListAct,mContext , "Explorer");

                    }
                });
       /* builder.setPositiveButton(R.string.profile_creator_save_success_scan_another,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      *//*  Intent intent = new Intent(ProfileCreatorActivity.this,
                                CameraReaderActivity.class);
                        startActivity(intent);
                        finish();*//*
                    }
                });
        */

        builder.create().show();
    }




    private Bitmap getBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = mContext.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = mContext.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

}
