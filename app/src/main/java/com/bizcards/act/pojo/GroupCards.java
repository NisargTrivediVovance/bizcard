package com.bizcards.act.pojo;

import com.bizcards.act.Profile;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class GroupCards {

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    private String group_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;


    public ArrayList<Profile> getmProfiles() {
        return mProfiles;
    }

    public void setmProfiles(ArrayList<Profile> mProfiles) {
        this.mProfiles = mProfiles;
    }

    private ArrayList<Profile>  mProfiles;

    public GroupCards(String strGroup_id, String strName, ArrayList<Profile> Profiles){
        this.group_id = strGroup_id;
        this.name = strName;
        this.mProfiles = Profiles;
    }



    public boolean isValid() {
        boolean isValid = StringUtils.isNotBlank(name);
        return isValid;
    }


}
