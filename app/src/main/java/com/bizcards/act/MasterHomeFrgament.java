package com.bizcards.act;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bizcards.act.adapter.ExpandableList_Home;
import com.bizcards.act.interfaceAll.GetDeleteId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

import static com.bizcards.act.MainActivity.drawer;
import static com.bizcards.act.MainActivity.masterFragmentManager1;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Address;
import static com.bizcards.act.ProfileDao.PROFILE_COL_COMPANY;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Date;
import static com.bizcards.act.ProfileDao.PROFILE_COL_EMAIL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_GroupId;
import static com.bizcards.act.ProfileDao.PROFILE_COL_ImagePath;
import static com.bizcards.act.ProfileDao.PROFILE_COL_IsFavorite;
import static com.bizcards.act.ProfileDao.PROFILE_COL_JOB_TITLE;
import static com.bizcards.act.ProfileDao.PROFILE_COL_NAME;
import static com.bizcards.act.ProfileDao.PROFILE_COL_PRIMARY_TEL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_WEB;

public class MasterHomeFrgament extends Fragment {

    Context mContext;

    public static ImageView imgMenu_group;
    ExpandableList_Home expalst_home_adp;

    private ProfileDao profileDao;
    List<Profile> mProfilist;

    public static int ProfileId;


    List<String> profileItems;
    ExpandableListView expandable_listview;


    public static ImageView imgMenu;
//    TextView  txtEmpty;

    ImageView imgsetting;

    TextView  tv_empty;

    EditText input_search;


    ExpandableList_Home   adpMainExpandable;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        mContext = MasterHomeFrgament.this.getActivity();

        imgsetting = (ImageView)  root.findViewById(R.id.imgsetting);
        input_search = (EditText) root.findViewById(R.id.input_search);

        expandable_listview = (ExpandableListView) root.findViewById(R.id.expandable_listview);

        imgsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Settings objSetting = new Settings();
                Utils.SetFragment(objSetting, mContext, "Settings");

            }
        });


        //
        List<Profile> mChildSearch = new ArrayList<>();

        input_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            String getsearch_txt;

            String HeaderSearch="";


                listDataHeader_search.clear();
                listDataChild_search.clear();

                mChildSearch.clear();
                if(input_search.getText().toString().length() == 0){

                    try {
                        expandable_listview.setAdapter(new ExpandableList_Home(MasterHomeFrgament.this.getActivity(), listDataHeader, listDataChild, new GetDeleteId() {
                            @Override
                            public void getMDeleteId(Profile Id) {

                                try {
                                    DeleteProcess(Id);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }));

                        for (int e = 0; e < listDataHeader.size(); e++) {
                            expandable_listview.expandGroup(e);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }else{

                    getsearch_txt = input_search.getText().toString().toLowerCase();
//                    Toast.makeText(mContext, "Search Text...."+getsearch_txt, Toast.LENGTH_SHORT).show();

                   // listDataChild_search


                    for( int H=0;H<listDataHeader.size();H++){

                        HeaderSearch = listDataHeader.get(H);


                        List<Profile> mprofile  = listDataChild.get(listDataHeader.get(H));

                        boolean  isMatch = false;
                        for(int e = 0;e < mprofile.size() ; e++){
                            //Profile childText  = listDataChild.get(listDataHeader.get(e).toString());
                            Profile profile  = mprofile.get(e);
                            String strEmail =  profile.getEmail().toString().toLowerCase();
                            String StrName = profile.getName().toString().toLowerCase();
                            String JobTitle  = profile.getJobTitle().toString().toLowerCase();

                            if(strEmail.contains(getsearch_txt) || StrName.contains(getsearch_txt) || JobTitle.contains(getsearch_txt)){

                                isMatch  = true;
                                mChildSearch.add(profile);
                            }
                            //  Profile childText = (Profile) getChild(groupPosition, childPosition);
                        }


                        if(isMatch) {
                            listDataHeader_search.add(HeaderSearch);
                        }
                    }


                    listDataChild_search.put(HeaderSearch,mChildSearch);


                    try {
                        expandable_listview.setAdapter(new ExpandableList_Home(MasterHomeFrgament.this.getActivity(), listDataHeader_search, listDataChild_search, new GetDeleteId() {
                            @Override
                            public void getMDeleteId(Profile Id) {

                                try {
                                    DeleteProcess(Id);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }));
                        for (int e = 0; e < listDataHeader_search.size(); e++) {
                            expandable_listview.expandGroup(e);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        });



        tv_empty = (TextView)  root.findViewById(R.id.tv_empty);
        imgMenu = (ImageView) root.findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }


            }
        });

        try {
            AsyncTaskExample asyncTask = new AsyncTaskExample();
            asyncTask.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return   root;

    }

//    ProgressDialog p;

    private class AsyncTaskExample extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            p = new ProgressDialog(mContext);
//            p.setMessage("Please wait..");
//            p.setIndeterminate(false);
//            p.setCancelable(false);
//            p.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                DoInBackground();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");
//            p.hide();

            try {

                       if(listDataHeader.size()>0){
                           tv_empty.setVisibility(View.GONE);
                            expandable_listview.setVisibility(View.VISIBLE);
                        }else {
                           tv_empty.setVisibility(View.VISIBLE);
                            expandable_listview.setVisibility(View.GONE);
                        }


            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                adpMainExpandable = (new ExpandableList_Home(MasterHomeFrgament.this.getActivity(), listDataHeader, listDataChild, new GetDeleteId() {
                    @Override
                    public void getMDeleteId(Profile Id) {

                        try {
                            DeleteProcess(Id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }));

                expandable_listview.setAdapter(adpMainExpandable);

               /* expandable_listview.setAdapter(new ExpandableList_Home(MasterHomeFrgament.this.getActivity(), listDataHeader, listDataChild, new GetDeleteId() {
                    @Override
                    public void getMDeleteId(Profile Id) {

                        try {
                            DeleteProcess(Id)
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }));*/

                for (int e = 0; e < listDataHeader.size(); e++) {
                    expandable_listview.expandGroup(e);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }




        }
    }


    private void DoInBackground() {


        profileDao = ProfileDao.getInstance(mContext);
        Cursor profileData = profileDao.loadDataForMinimalList();
        try {
//            CursorWindow cw = new CursorWindow("test", 5000);
//            AbstractWindowedCursor ac = (AbstractWindowedCursor) cursor;
        } catch (Exception e) {
            e.printStackTrace();
        }

        mProfilist = new ArrayList<>();
        if (profileData == null) {
            Utils.displayErrorDialog(mContext);

        } else {

            profileItems = new ArrayList<String>();

            for (profileData.moveToFirst(); !profileData.isAfterLast(); profileData.moveToNext()) {

                try {

                    byte[] image = new byte[0];
                    try {
//                        image = profileData.getBlob(11);
                    } catch (Exception e) {
                        e.printStackTrace();
//                        image = profileData.getBlob(profileData.getColumnIndex(PROFILE_COL_BlobImage));
                    }

                    Profile profile = new Profile("" + profileData.getString(0), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_NAME)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_COMPANY)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_EMAIL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_WEB)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Address)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Date)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_IsFavorite)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_GroupId)), image, "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_ImagePath)));
                    mProfilist.add(profile);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //profileItems.add(sb.toString());
                try {
                    profileItems.add(profileData.getString(2));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Log.d(ProfileListActivity.class.getName(), "Found " + profileItems.size() + " profiles in list");
            profileData.close();

        }

        prepareListData();
    }

    List<String> listDataHeader,listDataHeader_search;
    HashMap<String, List<Profile>> listDataChild,listDataChild_search;


    //==========   Expandable Process======================


    private void prepareListData() {


        String strcurrent_Date = "2020/06/22";

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Profile>>();

        listDataHeader_search = new ArrayList<String>();
        listDataChild_search = new HashMap<String, List<Profile>>();

        List<Profile> mChild;

        for (int i = 0; i < mProfilist.size(); i++) {

//            listDataHeader = new ArrayList<String>();

            Profile objP = mProfilist.get(i);
            String mDate = null;
            try {
                mDate = objP.getStrDate().toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(mDate)) {
                mDate = "2020/06/23";
            }

            if (!listDataHeader.contains(mDate)) {

                listDataHeader.add(mDate);
//                mChild.add(objP);
//                listDataChild.put(mDate, mChild);

            } else {


                try {
                    if (!TextUtils.isEmpty(mDate)) {
//                        listDataHeader.add(mDate);
                    } else {
//                        listDataHeader.add("No Date Set");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    listDataHeader.add("No Date Set");
                }

               /* try {
                    if (!TextUtils.isEmpty(mDate)) {
                        listDataHeader.add(mDate);
                    } else {
                        listDataHeader.add("No Date Set");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listDataHeader.add("No Date Set");
                }

                try {
                    mChild.add(objP);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

//                listDataChild.put(listDataHeader.get(0), home);
//               listDataChild.put(listDataHeader.get(0), home);
               /* try {
                    listDataChild.put(mDate, mChild);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
//               mChild.clear();
            }


        }


        String currentDate = "2020/06/22";

        for (int c = 0; c < listDataHeader.size(); c++) {


            String matchDate = listDataHeader.get(c);

            mChild = new ArrayList<>();

            for (int i = 0; i < mProfilist.size(); i++) {

                Profile objP = mProfilist.get(i);
                String mDate = null;
                try {
                    mDate = objP.getStrDate().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mDate.equalsIgnoreCase(matchDate)) {
//                    listDataHeader.add(mDate);
                    mChild.add(objP);
                    // listDataChild.put(mDate, mChild);

                } else {


                    try {
//                        mChild.add(objP);
                        //   listDataChild.put(mDate, mChild);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

               /* try {
                    if (!TextUtils.isEmpty(mDate)) {
                        listDataHeader.add(mDate);
                    } else {
                        listDataHeader.add("No Date Set");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listDataHeader.add("No Date Set");
                }

                try {
                    mChild.add(objP);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

//                listDataChild.put(listDataHeader.get(0), home);
//               listDataChild.put(listDataHeader.get(0), home);
               /* try {
                    listDataChild.put(mDate, mChild);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
//               mChild.clear();
                }


            }


            listDataChild.put(matchDate, mChild);
        }


//        String strDate[] = {"Today 2020/06/20", "2020/06/18", "2020/06/19"};

        // Adding headers
//        Resources res = getResources();
//        String[] headers = res.getStringArray(R.array.nav_drawer_labels);
//        listDataHeader = Arrays.asList(strDate);

        // Static method
//        List<String> home, friends, notifs;
//        String[] shome, sfriends, snotifs;

//        shome = res.getStringArray(R.array.elements_home);
//        home = profileItems;

//        sfriends = res.getStringArray(R.array.elements_friends);
//        friends = Arrays.asList(sfriends);

//        snotifs = res.getStringArray(R.array.elements_notifs);
//        notifs = Arrays.asList(snotifs);

        //Here we also need to add child values like group added value
        // Add to hashMap
//        listDataChild.put(listDataHeader.get(0), home); // Header, Child data
//        listDataChild.put(listDataHeader.get(1), friends);
//        listDataChild.put(listDataHeader.get(2), notifs);

//        listDataChild.put(listDataHeader.get(3), notifs);
//        listDataChild.put(listDataHeader.get(4), notifs);
//        listDataChild.put(listDataHeader.get(5), notifs);
//        listDataChild.put(listDataHeader.get(6), notifs);
//        listDataChild.put(listDataHeader.get(7), notifs);
//        listDataChild.put(listDataHeader.get(8), notifs);
//        listDataChild.put(listDataHeader.get(9), notifs);
//        listDataChild.put(listDataHeader.get(10), notifs);
    }


    @Override
    public void onResume() {
        super.onResume();

        //linearProfile_details.setVisibility(View.GONE);
//        card_view_Home.setVisibility(View.VISIBLE);


        try {
//            AsyncTaskExample asyncTask = new AsyncTaskExample();
//            asyncTask.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    private   void DeleteProcess(Profile mId){


        new AlertDialog.Builder(mContext)
                .setTitle("Delete Card Entry")
                .setMessage("Are you sure you want to delete this Card entry?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(R.string.dialog_Yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation


                        ProfileDao profileDao = null;
                        boolean isDelte = false;
                        try {
                            profileDao = ProfileDao.getInstance(mContext);

//                            int mId = Integer.parseInt(childText.getId());
                            isDelte = profileDao.deleteCard(Integer.parseInt(mId.getId()));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        if (isDelte) {
                            Toast.makeText(mContext, "Card Deleted Successful !", Toast.LENGTH_SHORT).show();


                            AsyncTaskExample  asynckTask =  new AsyncTaskExample();
                            asynckTask.execute("");
//                            listDataChild.remove(mId);
//                            try {
//                                adpMainExpandable.notifyDataSetChanged();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }


                        } else {
                            Toast.makeText(mContext, "Card could not Deleted Successful !", Toast.LENGTH_SHORT).show();
                        }

                        try {
                            profileDao.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(R.string.dialog_No, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }
}

