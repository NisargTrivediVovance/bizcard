package com.bizcards.act.ui.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.bizcards.act.CameraReaderActivity;
import com.bizcards.act.Groups_Fragment;
import com.bizcards.act.MainActivity;
import com.bizcards.act.Profile;
import com.bizcards.act.ProfileArrayAdapter;
import com.bizcards.act.ProfileDao;
import com.bizcards.act.ProfileListActivity;
import com.bizcards.act.R;
import com.bizcards.act.Settings;
import com.bizcards.act.Utils;
import com.bizcards.act.adapter.ExpandableList_Home;
import com.bizcards.act.adapter.GroupFragAdapter;
import com.bizcards.act.adapter.NavAdapter;
import com.bizcards.act.pojo.GroupCards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

//import static com.bizcards.act.MainActivity.card_view_Home;
import static com.bizcards.act.MainActivity.drawer;
import static com.bizcards.act.MainActivity.linearFooter;
//import static com.bizcards.act.MainActivity.linearProfile_details;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Address;
import static com.bizcards.act.ProfileDao.PROFILE_COL_BlobImage;
import static com.bizcards.act.ProfileDao.PROFILE_COL_COMPANY;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Date;
import static com.bizcards.act.ProfileDao.PROFILE_COL_EMAIL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_GroupId;
import static com.bizcards.act.ProfileDao.PROFILE_COL_ImagePath;
import static com.bizcards.act.ProfileDao.PROFILE_COL_IsFavorite;
import static com.bizcards.act.ProfileDao.PROFILE_COL_JOB_TITLE;
import static com.bizcards.act.ProfileDao.PROFILE_COL_NAME;
import static com.bizcards.act.ProfileDao.PROFILE_COL_PRIMARY_TEL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_WEB;

public class HomeFragment extends Fragment {

//    private HomeViewModel homeViewModel;


//    private Button profileListButton;
//    private Button cameraReaderButton;

    Context mContext;
    //    TextView  tvScan;
    private ProfileDao profileDao;
    List<Profile> mProfilist;

    public static int ProfileId;


    List<String> profileItems;
    ExpandableListView expandable_listview;


    public static ImageView imgMenu;
//    TextView  txtEmpty;

    ImageView imgsetting;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       /* homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);*/
        View root = inflater.inflate(R.layout.fragment_home, container, false);

//         txtEmpty = (TextView) root.findViewById(R.id.txtEmpty);


//        final TextView textView = root.findViewById(R.id.text_home);
      /*  homeViewModel.getText().observe(HomeFragment.this.getActivity(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
*/

//        tvScan = (TextView) root.findViewById(R.id.tvScan);

//        profileListButton = (Button) root.findViewById(R.id.profileListButton);
//        cameraReaderButton = (Button) root.findViewById(R.id.cameraReaderButton);

        mContext = HomeFragment.this.getActivity();

//        fab.setVisibility(View.GONE);

       /* profileListButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ProfileListActivity.class);
                startActivity(intent);
            }
        });
        cameraReaderButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CameraReaderActivity.class);
                startActivity(intent);
            }
        });*/

        /*tvScan.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CameraReaderActivity.class);
                startActivity(intent);
            }
        });
*/


        imgsetting = (ImageView)  root.findViewById(R.id.imgsetting);

        imgsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Settings objSetting = new Settings();
                Utils.SetFragment(objSetting, mContext, "Settings");

            }
        });


        imgMenu = (ImageView) root.findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }


            }
        });

        expandable_listview = (ExpandableListView) root.findViewById(R.id.expandable_listview);

        try {
            AsyncTaskExample asyncTask = new AsyncTaskExample();
            asyncTask.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }


        return root;
    }


    ProgressDialog p;

    private class AsyncTaskExample extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            p = new ProgressDialog(mContext);
            p.setMessage("Please wait..");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            DoInBackground();

            return "";
        }

        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");
            p.hide();



            try {

                       /* if(listDataHeader.size()>0){
                            txtEmpty.setVisibility(View.GONE);
                            expandable_listview.setVisibility(View.VISIBLE);
                        }else {
                            txtEmpty.setVisibility(View.VISIBLE);
                            expandable_listview.setVisibility(View.GONE);
                        }
*/
             //   expandable_listview.setAdapter(new ExpandableList_Home(HomeFragment.this.getActivity(), listDataHeader, listDataChild));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                for (int e = 0; e < listDataHeader.size(); e++) {
                    expandable_listview.expandGroup(e);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }




        }
    }


    private void DoInBackground() {


        profileDao = ProfileDao.getInstance(mContext);
        Cursor profileData = profileDao.loadDataForMinimalList();
        try {
//            CursorWindow cw = new CursorWindow("test", 5000);
//            AbstractWindowedCursor ac = (AbstractWindowedCursor) cursor;
        } catch (Exception e) {
            e.printStackTrace();
        }

        mProfilist = new ArrayList<>();
        if (profileData == null) {
            Utils.displayErrorDialog(mContext);

        } else {
            profileItems = new ArrayList<String>();
            for (profileData.moveToFirst(); !profileData.isAfterLast(); profileData.moveToNext()) {


                try {

                   /* result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                            result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Date))
                            */

                    // Profile profile = new Profile("" + profileData.getString(0), "" + profileData.getString(1), "" + profileData.getString(2), "" + profileData.getString(4), "" + profileData.getString(5), "" + profileData.getString(3), "" + profileData.getString(6), "" + profileData.getString(7),"" + profileData.getString(8));

                    byte[] image = new byte[0];
                    try {
//                        image = profileData.getBlob(11);
                    } catch (Exception e) {
                        e.printStackTrace();
//                        image = profileData.getBlob(profileData.getColumnIndex(PROFILE_COL_BlobImage));
                    }
                    Profile profile = new Profile("" + profileData.getString(0), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_NAME)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_COMPANY)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_EMAIL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_WEB)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Address)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Date)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_IsFavorite)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_GroupId)), image, "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_ImagePath)));
                    mProfilist.add(profile);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //profileItems.add(sb.toString());
                try {
                    profileItems.add(profileData.getString(2));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Log.d(ProfileListActivity.class.getName(), "Found " + profileItems.size() + " profiles in list");
            profileData.close();

        }

        prepareListData();
    }

    List<String> listDataHeader;
    HashMap<String, List<Profile>> listDataChild;

    //==========   Expandable Process======================


    private void prepareListData() {


        String strcurrent_Date = "2020/06/22";

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Profile>>();

        List<Profile> mChild;

        for (int i = 0; i < mProfilist.size(); i++) {

//            listDataHeader = new ArrayList<String>();

            Profile objP = mProfilist.get(i);
            String mDate = null;
            try {
                mDate = objP.getStrDate().toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(mDate)) {
                mDate = "2020/06/23";
            }

            if (!listDataHeader.contains(mDate)) {

                listDataHeader.add(mDate);
//                mChild.add(objP);
//                listDataChild.put(mDate, mChild);

            } else {


                try {
                    if (!TextUtils.isEmpty(mDate)) {
//                        listDataHeader.add(mDate);
                    } else {
//                        listDataHeader.add("No Date Set");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    listDataHeader.add("No Date Set");
                }

               /* try {
                    if (!TextUtils.isEmpty(mDate)) {
                        listDataHeader.add(mDate);
                    } else {
                        listDataHeader.add("No Date Set");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listDataHeader.add("No Date Set");
                }

                try {
                    mChild.add(objP);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

//                listDataChild.put(listDataHeader.get(0), home);
//               listDataChild.put(listDataHeader.get(0), home);
               /* try {
                    listDataChild.put(mDate, mChild);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
//               mChild.clear();
            }


        }


        String currentDate = "2020/06/22";

        for (int c = 0; c < listDataHeader.size(); c++) {


            String matchDate = listDataHeader.get(c);

            mChild = new ArrayList<>();

            for (int i = 0; i < mProfilist.size(); i++) {

                Profile objP = mProfilist.get(i);
                String mDate = null;
                try {
                    mDate = objP.getStrDate().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mDate.equalsIgnoreCase(matchDate)) {
//                    listDataHeader.add(mDate);
                    mChild.add(objP);
                    // listDataChild.put(mDate, mChild);

                } else {


                    try {
//                        mChild.add(objP);
                        //   listDataChild.put(mDate, mChild);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

               /* try {
                    if (!TextUtils.isEmpty(mDate)) {
                        listDataHeader.add(mDate);
                    } else {
                        listDataHeader.add("No Date Set");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listDataHeader.add("No Date Set");
                }

                try {
                    mChild.add(objP);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

//                listDataChild.put(listDataHeader.get(0), home);
//               listDataChild.put(listDataHeader.get(0), home);
               /* try {
                    listDataChild.put(mDate, mChild);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
//               mChild.clear();
                }


            }


            listDataChild.put(matchDate, mChild);
        }


//        String strDate[] = {"Today 2020/06/20", "2020/06/18", "2020/06/19"};

        // Adding headers
        Resources res = getResources();
//        String[] headers = res.getStringArray(R.array.nav_drawer_labels);
//        listDataHeader = Arrays.asList(strDate);

        // Static method
        List<String> home, friends, notifs;
//        String[] shome, sfriends, snotifs;

//        shome = res.getStringArray(R.array.elements_home);
//        home = profileItems;

//        sfriends = res.getStringArray(R.array.elements_friends);
//        friends = Arrays.asList(sfriends);

//        snotifs = res.getStringArray(R.array.elements_notifs);
//        notifs = Arrays.asList(snotifs);

        //Here we also need to add child values like group added value
        // Add to hashMap
//        listDataChild.put(listDataHeader.get(0), home); // Header, Child data
//        listDataChild.put(listDataHeader.get(1), friends);
//        listDataChild.put(listDataHeader.get(2), notifs);

//        listDataChild.put(listDataHeader.get(3), notifs);
//        listDataChild.put(listDataHeader.get(4), notifs);
//        listDataChild.put(listDataHeader.get(5), notifs);
//        listDataChild.put(listDataHeader.get(6), notifs);
//        listDataChild.put(listDataHeader.get(7), notifs);
//        listDataChild.put(listDataHeader.get(8), notifs);
//        listDataChild.put(listDataHeader.get(9), notifs);
//        listDataChild.put(listDataHeader.get(10), notifs);
    }



}