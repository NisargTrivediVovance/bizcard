package com.bizcards.act;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bizcards.act.adapter.ExpandableList_Home;
import com.bizcards.act.adapter.FavoriteAdapter;
import com.bizcards.act.interfaceAll.GetDeleteId;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.bizcards.act.MainActivity.drawer;
//import static com.bizcards.act.MainActivity.linearProfile_details;

public class Favourites_Fragment extends Fragment {

    Context mContext;


    ImageView img_Settings;


    //CardView  card_view8;

    RecyclerView list_of_new_group;

    LinearLayoutManager mLayoutManager;


    FavoriteAdapter favoriteadp;



    ProfileDao profileDao;

    public static ImageView imgMenu_group;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favourites, container, false);

        mContext = Favourites_Fragment.this.getActivity();
//        img_plus = (ImageButton) root.findViewById(R.id.img_plus);
        list_of_new_group = (RecyclerView) root.findViewById(R.id.list_of_new_group);
        imgMenu_group = (ImageView)root.findViewById(R.id.imgMenu_group);
        img_Settings = (ImageView)  root.findViewById(R.id.img_Settings);


        img_Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Settings objSetting = new Settings();
                Utils.SetFragment(objSetting, mContext, "Settings");

            }
        });

        imgMenu_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });





        return root;
    }


    @Override
    public void onResume() {
        super.onResume();

        AsyncFavorite asynckTask =  new AsyncFavorite();
        asynckTask.execute("");

        //FavoriteListingProcess();

        //linearProfile_details.setVisibility(View.GONE);
    }



    ArrayList<Profile>   mFavoriteList;

    private void FavoriteListingProcess(){

        profileDao = ProfileDao.getInstance(mContext);
        mFavoriteList = profileDao.loadFavorites();
//        boolean isupdate = profileDao.update(profile);

    }


    private void setUp(ArrayList<Profile>  getlist) {

        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        list_of_new_group.setLayoutManager(mLayoutManager);
        list_of_new_group.setItemAnimator(new DefaultItemAnimator());

//        ArrayList<String> grpdata = new ArrayList<>();
//        grpdata.add("Business");
//        grpdata.add("Family");
//        grpdata.add("Vip");
//        grpdata.add("Colleagues");
//        grpdata.add("Customer");

        favoriteadp = new FavoriteAdapter(mContext, getlist, new GetDeleteId() {
            @Override
            public void getMDeleteId(Profile Id) {

                DeleteProcess(Id);

            }
        });

        list_of_new_group.setAdapter(favoriteadp);
        // prepareDemoContent();
    }



    private   void DeleteProcess(Profile mId){


        new AlertDialog.Builder(mContext)
                .setTitle("Delete Card Entry")
                .setMessage("Are you sure you want to delete this Card entry?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(R.string.dialog_Yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation


                        ProfileDao profileDao = null;
                        boolean isDelte = false;
                        try {
                            profileDao = ProfileDao.getInstance(mContext);

//                            int mId = Integer.parseInt(childText.getId());
                            isDelte = profileDao.deleteCard(Integer.parseInt(mId.getId()));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        if (isDelte) {
                            Toast.makeText(mContext, "Card Deleted Successful !", Toast.LENGTH_SHORT).show();


                            AsyncFavorite asynckTask =  new AsyncFavorite();
                            asynckTask.execute("");
//                            listDataChild.remove(mId);
//                            try {
//                                adpMainExpandable.notifyDataSetChanged();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }


                        } else {
                            Toast.makeText(mContext, "Card could not Deleted Successful !", Toast.LENGTH_SHORT).show();
                        }

                        try {
                            profileDao.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(R.string.dialog_No, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }




    private class AsyncFavorite extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            p = new ProgressDialog(mContext);
//            p.setMessage("Please wait..");
//            p.setIndeterminate(false);
//            p.setCancelable(false);
//            p.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            FavoriteListingProcess();

            return "";
        }

        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");
//            p.hide();

            try {

                setUp(mFavoriteList);

            } catch (Exception e) {
                e.printStackTrace();
            }




        }
    }



}
