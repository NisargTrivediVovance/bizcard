package com.bizcards.act;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bizcards.act.adapter.GroupFragAdapter;
import com.bizcards.act.pojo.GroupCards;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.bizcards.act.MainActivity.drawer;
//import static com.bizcards.act.MainActivity.linearProfile_details;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Address;
import static com.bizcards.act.ProfileDao.PROFILE_COL_COMPANY;
import static com.bizcards.act.ProfileDao.PROFILE_COL_Date;
import static com.bizcards.act.ProfileDao.PROFILE_COL_EMAIL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_JOB_TITLE;
import static com.bizcards.act.ProfileDao.PROFILE_COL_NAME;
import static com.bizcards.act.ProfileDao.PROFILE_COL_PRIMARY_TEL;
import static com.bizcards.act.ProfileDao.PROFILE_COL_WEB;

public class Groups_Fragment extends Fragment {

    Context mContext;


    ImageButton img_plus;
    ImageView imgMenu_group;

    //CardView  card_view8;

    RecyclerView list_of_new_group;
    LinearLayoutManager mLayoutManager;
    GroupFragAdapter grpadpter;
    ProfileDao profileDao;

    ArrayList<GroupCards> grpdata;

    ArrayList<String> grouNameMatching;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_groups, container, false);

        mContext = Groups_Fragment.this.getActivity();
        profileDao = ProfileDao.getInstance(mContext);

        imgMenu_group = (ImageView) root.findViewById(R.id.imgMenu_group);

        img_plus = (ImageButton) root.findViewById(R.id.img_plus);
        list_of_new_group = (RecyclerView) root.findViewById(R.id.list_of_new_group);


        img_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CreateGroup();
//                Toast.makeText(mContext, "inProcesss", Toast.LENGTH_SHORT).show();
            }
        });


        imgMenu_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });




        AsyncTaskExample asyncTask=new AsyncTaskExample();
        asyncTask.execute("");




        try {
            //getGroupList();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return root;
    }


    ProgressDialog p;

    private class AsyncTaskExample extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p = new ProgressDialog(mContext);
            p.setMessage("Please wait..");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();

            setUp();
        }
        @Override
        protected String doInBackground(String... strings) {

            DoInBackground();

            return "";
        }
        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute("");

            p.hide();

            grpadpter = new GroupFragAdapter(mContext, grpdata);
            list_of_new_group.setAdapter(grpadpter);

        }
    }



    private void DoInBackground(){

        try {
            getGroupList();
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!grouNameMatching.contains("Business")) {

            try {

                ArrayList<Profile>  BProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Business", BProfiles);
                saveGroup(groupCard);
                //validateAndCreateGroup("Business");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Family")) {

            try {

                ArrayList<Profile>  FProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Family", FProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Family");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Vip")) {

            try {

                ArrayList<Profile>  VProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Vip", VProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Vip");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Colleagues")) {

            try {

                ArrayList<Profile>  CProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Colleagues", CProfiles);
                saveGroup(groupCard);
                //validateAndCreateGroup("Colleagues");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!grouNameMatching.contains("Customer")) {

            try {

                ArrayList<Profile>  CUSProfiles = new ArrayList<>();
                GroupCards groupCard = new GroupCards("gid", "Customer", CUSProfiles);
                saveGroup(groupCard);

                //validateAndCreateGroup("Customer");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void setUp() {


        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        list_of_new_group.setLayoutManager(mLayoutManager);
        list_of_new_group.setItemAnimator(new DefaultItemAnimator());
        //grpdata.add("Business");
        //grpdata.add("Family");
        //grpdata.add("Vip");
        //grpdata.add("Colleagues");
        //grpdata.add("Customer");




        // prepareDemoContent();
    }


    private void CreateGroup() {

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Group");
        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);
        // add a button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // send data from the AlertDialog to the Activity
                EditText editText = customLayout.findViewById(R.id.editText);
//                sendDialogDataToActivity(editText.getText().toString());

                String groupName = editText.getText().toString();
                try {
                    validateAndCreateGroup(groupName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private boolean saveGroup(GroupCards objgroup) {
        return profileDao.insertGroup(objgroup);
    }


    private void validateAndCreateGroup(String groupName) {

        ArrayList<Profile>  PProfiles = new ArrayList<>();
        GroupCards groupCard = new GroupCards("gid", groupName, PProfiles);
        if (groupCard.isValid()) {
            if (saveGroup(groupCard)) {

                try {
                    //setUp();

                    AsyncTaskExample asyncTask=new AsyncTaskExample();
                    asyncTask.execute("");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                showSaveSuccessDialog();
            } else {
                Utils.displayErrorDialog(mContext);
            }
        } else {
            alertInvalidProfile();
        }
    }


    private void alertInvalidProfile() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.profile_creator_save_invalid_title);
        builder.setMessage(R.string.profile_creator_save_invalid_message);
        builder.setPositiveButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        builder.create().show();
    }

    private void showSaveSuccessDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.dialog_success);
        builder.setMessage(R.string.group_creator_save_success_message);
        builder.setCancelable(false); //Don't let them touch out!
        builder.setNegativeButton(R.string.dialog_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       /* Intent intent = new Intent(mContext,
                                ProfileListActivity.class);
                        startActivity(intent);*/
                        dialogInterface.dismiss();
                    }
                });

        builder.create().show();
    }

    public void getGroupList() {


        grpdata = new ArrayList<GroupCards>();
        grouNameMatching = new ArrayList<>();

        Cursor groupData = profileDao.loadDataForGroup();

        if (groupData == null) {

            Utils.displayErrorDialog(mContext);

        } else {

            for (groupData.moveToFirst(); !groupData.isAfterLast(); groupData.moveToNext()) {


                try {


                   /* result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                            result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                            result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                            result.getString(result.getColumnIndex(PROFILE_COL_Date))
                            */

                    // Profile profile = new Profile("" + profileData.getString(0), "" + profileData.getString(1), "" + profileData.getString(2), "" + profileData.getString(4), "" + profileData.getString(5), "" + profileData.getString(3), "" + profileData.getString(6), "" + profileData.getString(7),"" + profileData.getString(8));

                    //GroupCards profile = new GroupCards("" + groupData.getString(0), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_NAME)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_COMPANY)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_EMAIL)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_WEB)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Address)), "" + profileData.getString(profileData.getColumnIndex(PROFILE_COL_Date)));


                    //Here we get Actule Dataas
                    ArrayList<Profile>  getLiveProfils = profileDao.loadGroupCount( groupData.getString(0));
                    GroupCards groupCards = new GroupCards("" + groupData.getString(0), "" + groupData.getString(1), getLiveProfils);
                    String groupname = groupData.getString(1);

                    grouNameMatching.add(groupname);
                    grpdata.add(groupCards);
                    //grpadpter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            Log.d(ProfileListActivity.class.getName(), "Found " + profileItems.size() + " profiles in list");

        }

    }


    @Override
    public void onResume() {
        super.onResume();

        //linearProfile_details.setVisibility(View.GONE);

    }
}
