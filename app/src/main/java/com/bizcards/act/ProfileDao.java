package com.bizcards.act;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.bizcards.act.pojo.GroupCards;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Koha Choji on 08/06/2017.
 */

public class ProfileDao extends SQLiteOpenHelper {

    private static final String TAG = ProfileDao.class.getName();

    private static final String APP_DATABASE = "miller_bcr.db";
    private static final String PROFILE_TABLE = "profiles";
    private static final String PROFILE_COL_ID = "id";
    public static final String PROFILE_COL_NAME = "name";
    public static final String PROFILE_COL_JOB_TITLE = "job_title";
    public static final String PROFILE_COL_COMPANY = "company";
    public static final String PROFILE_COL_PRIMARY_TEL = "primary_tel";
    public static final String PROFILE_COL_EMAIL = "email";
    public static final String PROFILE_COL_WEB = "mweb";
    public static final String PROFILE_COL_Address = "address";
    public static final String PROFILE_COL_Date = "date";
    public static final String PROFILE_COL_IsFavorite = "isFavorite";

    public static final String PROFILE_COL_GroupId = "isGroupId";
    public static final String PROFILE_COL_BlobImage = "BlobImage";
    public static final String PROFILE_COL_ImagePath = "ImagePath";



    private static ProfileDao mInstance;

    static final String GROUP_TABLE = "Group_cards";
    public static final String GROUP_COL_NAME = "group_name";
    public static final String GROUP_COL_Card_id = "card_id";




    public static final String GROUP_COL_SETTING = "group_setting";
    static final String GROUP_SETTING_TABLE = "Group_Settings";
    private ProfileDao(Context context) {
        super(context, APP_DATABASE, null, 1);
    }

    public static ProfileDao getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ProfileDao(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        StringBuilder createTableQueryBuilder = new StringBuilder();

/*
        createTableQueryBuilder.append("create table ").append(PROFILE_TABLE)
                .append(" (")
                .append(PROFILE_COL_ID).append(" integer primary key")
                .append(", ")
                .append(PROFILE_COL_NAME).append(" text")
                .append(", ")
                .append(PROFILE_COL_JOB_TITLE).append(" text")
                .append(", ")
                .append(PROFILE_COL_COMPANY).append(" text")
                .append(", ")
                .append(PROFILE_COL_PRIMARY_TEL).append(" text")
                .append(", ")
                .append(PROFILE_COL_EMAIL).append(" text")
                .append(", ")
                .append(PROFILE_COL_WEB).append(" text")
                .append(", ")
                .append(PROFILE_COL_Address).append(" text")
                .append(", ")
                .append(PROFILE_COL_Date).append(" text")



       */
/* private static final String PROFILE_COL_WEB = "mweb";
        private static final String PROFILE_COL_Address = "address";
        private static final String PROFILE_COL_Date = "date";
        *//*

                .append(")");
        Log.d("","--"+createTableQueryBuilder.toString());
        sqLiteDatabase.execSQL(createTableQueryBuilder.toString());

*/


       /* private static final String PROFILE_COL_NAME = "name";
        private static final String PROFILE_COL_JOB_TITLE = "job_title";
        private static final String PROFILE_COL_COMPANY = "company";
        private static final String PROFILE_COL_PRIMARY_TEL = "primary_tel";
        private static final String PROFILE_COL_EMAIL = "email";
        private static final String PROFILE_COL_WEB = "mweb";
        private static final String PROFILE_COL_Address = "address";
        private static final String PROFILE_COL_Date = "date";

        */
        sqLiteDatabase.execSQL(
                "create table  " + PROFILE_TABLE + "(id integer primary key, " + PROFILE_COL_NAME + " text," + PROFILE_COL_JOB_TITLE + " text ," + PROFILE_COL_COMPANY + " text," + PROFILE_COL_PRIMARY_TEL + " text ," + PROFILE_COL_EMAIL + "  text," + PROFILE_COL_WEB + " text," + PROFILE_COL_Address + " text," + PROFILE_COL_Date + " text," + PROFILE_COL_IsFavorite + " text," + PROFILE_COL_GroupId + " text," + PROFILE_COL_BlobImage + " blob," + PROFILE_COL_ImagePath + " text)"
        );


        sqLiteDatabase.execSQL(
                "create table  " + GROUP_TABLE + "(id integer primary key, " + GROUP_COL_NAME + " text," + GROUP_COL_Card_id + " text)"
        );

        sqLiteDatabase.execSQL(
                "create table  " + GROUP_SETTING_TABLE + "(id integer primary key, " + GROUP_COL_SETTING + " text)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //TODO implement a proper upgrade task
        // (e.g. backup old database and insert to new schema)

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(sqLiteDatabase);
    }

    public boolean insert(Profile profile) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            setContentValuesFromProfile(values, profile);

            db.insert(PROFILE_TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return false;
        }
        return true;
    }



    public boolean insertGroupSetting(String strSetting) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            setContentValuesForGroupSetting(values, strSetting);
            db.insert(GROUP_SETTING_TABLE, null, values);
//            db.close();
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return false;
        }
        return true;
    }

    //

    public boolean insertGroup(GroupCards objGroup) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            setContentValuesFromGroup(values, objGroup);
            db.insert(GROUP_TABLE, null, values);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return false;
        }

        return true;
    }


    public boolean update(Profile profile) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            setContentValuesFromProfile(values, profile);
            db.update(PROFILE_TABLE, values, "id = ?",
                    new String[]{String.valueOf(profile.getId())});
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return false;
        }
        return true;
    }




    private void setContentValuesFromProfile(ContentValues values, Profile profile) {

        values.put(PROFILE_COL_NAME, profile.getName());
        values.put(PROFILE_COL_JOB_TITLE, profile.getJobTitle());
        values.put(PROFILE_COL_COMPANY, profile.getCompany());
        values.put(PROFILE_COL_PRIMARY_TEL, profile.getPrimaryContactNumber());
        values.put(PROFILE_COL_EMAIL, profile.getEmail());
        values.put(PROFILE_COL_WEB, profile.getStrWeb());
        values.put(PROFILE_COL_Address, profile.getStrAddress());
        values.put(PROFILE_COL_Date, profile.getStrDate());
        values.put(PROFILE_COL_IsFavorite, profile.getIsFavorite());
        values.put(PROFILE_COL_GroupId, profile.getGroupId());
//        values.put(PROFILE_COL_BlobImage,profile.getImageBlob());
        values.put(PROFILE_COL_ImagePath,profile.getImagepath());
    }



    private void setContentValuesForGroupSetting(ContentValues values, String objgroup) {
        values.put(GROUP_COL_SETTING, objgroup);
        //values.put(GROUP_COL_Card_id, objgroup.getCard_id());

    }

    private void setContentValuesFromGroup(ContentValues values, GroupCards objgroup) {
        values.put(GROUP_COL_NAME, objgroup.getName());
        //values.put(GROUP_COL_Card_id, objgroup.getCard_id());

    }

    public Cursor loadDataForMinimalList() {

        try {

            SQLiteDatabase db = this.getReadableDatabase();
/*
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("select * ")
//                    .append(PROFILE_COL_ID).append(", ")
//                    .append(PROFILE_COL_NAME).append(", ")
//                    .append(PROFILE_COL_COMPANY)

//                    .append(PROFILE_COL_JOB_TITLE)
//                    .append(PROFILE_COL_EMAIL)

                    .append(" from ").append(PROFILE_TABLE)
                    .append(" order by ").append(PROFILE_COL_NAME).append(" asc")
                    .append(";");*/


            Cursor result = db.rawQuery("select * from  " + PROFILE_TABLE, null);

            /*Cursor result =
                    db.rawQuery(queryBuilder.toString(), null);
*/

            return result;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        return null;
    }

    public Profile load(Integer id) {

        Profile profile = null;

        if (id != null) {
            try {

                SQLiteDatabase db = this.getReadableDatabase();

                StringBuilder queryBuilder = new StringBuilder();

                queryBuilder.append("select * ")
                        /*.append(PROFILE_COL_NAME).append(", ")
                        .append(PROFILE_COL_JOB_TITLE).append(", ")
                        .append(PROFILE_COL_COMPANY).append(", ")

                        .append(PROFILE_COL_PRIMARY_TEL).append(", ")
                        .append(PROFILE_COL_EMAIL).append(", ")

                        .append(PROFILE_COL_WEB).append(", ")
                        .append(PROFILE_COL_Address).append(", ")
                        .append(PROFILE_COL_Date)*/

                        .append(" from ").append(PROFILE_TABLE)
                        .append(" where ").append(PROFILE_COL_ID).append(" = ?")
                        .append(";");

               /* Cursor result =
                        db.rawQuery(queryBuilder.toString(), new String[]{String.valueOf(id)});*/





               /* result.getString(result.getColumnIndex("id")),
                        result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                        result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)),
                        result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)),
                        result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Date)),
                        result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite))*/


                //-----------------Added Code--------

                String[] settingsProjection = {
                        "id",
                        PROFILE_COL_NAME,
                        PROFILE_COL_JOB_TITLE,
                        PROFILE_COL_COMPANY,
                        PROFILE_COL_PRIMARY_TEL,
                        PROFILE_COL_EMAIL,
                        PROFILE_COL_WEB,
                        PROFILE_COL_Address,
                        PROFILE_COL_Date,
                        PROFILE_COL_IsFavorite,
                        PROFILE_COL_GroupId,
                        PROFILE_COL_BlobImage,
                        PROFILE_COL_ImagePath

                };

                String whereClause = "id" + "=?";
                String[] whereArgs = {id.toString()};

                Cursor result = db.query(
                        PROFILE_TABLE,
                        settingsProjection,
                        whereClause,
                        whereArgs,
                        null,
                        null,
                        null
                );

                //---------------------Added code end

                result.moveToFirst();

               /* profile = new Profile(

                        result.getString(result.getColumnIndex("id")),
                        result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                        result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)),
                        result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)),
                        result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Date)),
                        result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite))

                );*/

               // profile = new Profile("" + result.getString(0), "" + result.getString(result.getColumnIndex(PROFILE_COL_NAME)), "" + result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)), "" + result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)), "" + result.getString(result.getColumnIndex(PROFILE_COL_WEB)), "" + result.getString(result.getColumnIndex(PROFILE_COL_Address)), "" + result.getString(result.getColumnIndex(PROFILE_COL_Date)), "" + result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite)), "" + result.getString(result.getColumnIndex(PROFILE_COL_GroupId)));

                byte[] image = result.getBlob(11);
//
                profile = new Profile("" + result.getString(result.getColumnIndex("id")), "" + result.getString(result.getColumnIndex(PROFILE_COL_NAME)), "" + result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)), "" + result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)), "" + result.getString(result.getColumnIndex(PROFILE_COL_WEB)), "" + result.getString(result.getColumnIndex(PROFILE_COL_Address)), "" + result.getString(result.getColumnIndex(PROFILE_COL_Date)), "" + result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite)), "" + result.getString(result.getColumnIndex(PROFILE_COL_GroupId)) ,image,""+result.getString(result.getColumnIndex(PROFILE_COL_ImagePath)));


                profile.setId(String.valueOf(id));
                result.close();

            } catch (Exception e) {
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
        return profile;
    }

    public Cursor loadDataForGroup() {

        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor result = db.rawQuery("select * from  " + GROUP_TABLE, null);
            return result;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        return null;
    }

    public ArrayList<Profile> loadFavorites() {


        ArrayList<Profile> favorite_profile = new ArrayList<>();


        Profile profile = null;

        try {

            SQLiteDatabase db = this.getReadableDatabase();

//            StringBuilder queryBuilder = new StringBuilder();

//            queryBuilder.append("select * ")
                    /*.append(PROFILE_COL_NAME).append(", ")
                    .append(PROFILE_COL_JOB_TITLE).append(", ")
                    .append(PROFILE_COL_COMPANY).append(", ")

                    .append(PROFILE_COL_PRIMARY_TEL).append(", ")
                    .append(PROFILE_COL_EMAIL).append(", ")

                    .append(PROFILE_COL_WEB).append(", ")
                    .append(PROFILE_COL_Address).append(", ")
                    .append(PROFILE_COL_Date)*/

//                    .append(" from ").append(PROFILE_TABLE)
//                    .append(" where ").append(PROFILE_COL_ID).append(" = ?")
//                    .append(";");

               /* Cursor result =
                        db.rawQuery(queryBuilder.toString(), new String[]{String.valueOf(id)});*/





               /* result.getString(result.getColumnIndex("id")),
                        result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                        result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)),
                        result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)),
                        result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Date)),
                        result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite))*/


            //-----------------Added Code--------

            String[] settingsProjection = {
                    "id",
                    PROFILE_COL_NAME,
                    PROFILE_COL_JOB_TITLE,
                    PROFILE_COL_COMPANY,
                    PROFILE_COL_PRIMARY_TEL,
                    PROFILE_COL_EMAIL,
                    PROFILE_COL_WEB,
                    PROFILE_COL_Address,
                    PROFILE_COL_Date,
                    PROFILE_COL_IsFavorite,
                    PROFILE_COL_GroupId,
                    PROFILE_COL_BlobImage,
                    PROFILE_COL_ImagePath

            };

            String whereClause = PROFILE_COL_IsFavorite + "=?";
            String[] whereArgs = {"true"};

            Cursor result = db.query(
                    PROFILE_TABLE,
                    settingsProjection,
                    whereClause,
                    whereArgs,
                    null,
                    null,
                    null
            );




            //---------------------Added code end


            result.moveToFirst();

            while(result.isAfterLast() == false){


                String name =  ""+ result.getString(result.getColumnIndex(PROFILE_COL_NAME));

                byte[] image = result.getBlob(11);
//
                profile = new Profile("" + result.getString(result.getColumnIndex("id")), "" + result.getString(result.getColumnIndex(PROFILE_COL_NAME)), "" + result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)), "" + result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)), "" + result.getString(result.getColumnIndex(PROFILE_COL_WEB)), "" + result.getString(result.getColumnIndex(PROFILE_COL_Address)), "" + result.getString(result.getColumnIndex(PROFILE_COL_Date)), "" + result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite)), "" + result.getString(result.getColumnIndex(PROFILE_COL_GroupId)) ,image,""+result.getString(result.getColumnIndex(PROFILE_COL_ImagePath)));

                favorite_profile.add(profile);

                result.moveToNext();
//                profile.setId(String.valueOf(id));
            }

            result.close();


        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        return favorite_profile;
    }

    public ArrayList<Profile> loadGroupCount(String groupId) {


        ArrayList<Profile> favorite_profile = new ArrayList<>();
        Profile profile = null;

        try {

            SQLiteDatabase db = this.getReadableDatabase();

            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.append("select * ")
                    /*.append(PROFILE_COL_NAME).append(", ")
                    .append(PROFILE_COL_JOB_TITLE).append(", ")
                    .append(PROFILE_COL_COMPANY).append(", ")

                    .append(PROFILE_COL_PRIMARY_TEL).append(", ")
                    .append(PROFILE_COL_EMAIL).append(", ")

                    .append(PROFILE_COL_WEB).append(", ")
                    .append(PROFILE_COL_Address).append(", ")
                    .append(PROFILE_COL_Date)*/

                    .append(" from ").append(PROFILE_TABLE)
                    .append(" where ").append(PROFILE_COL_GroupId).append(" = ?")
                    .append(";");

               /* Cursor result =
                        db.rawQuery(queryBuilder.toString(), new String[]{String.valueOf(id)});*/


               /* result.getString(result.getColumnIndex("id")),
                        result.getString(result.getColumnIndex(PROFILE_COL_NAME)),
                        result.getString(result.getColumnIndex(PROFILE_COL_JOB_TITLE)),
                        result.getString(result.getColumnIndex(PROFILE_COL_COMPANY)),
                        result.getString(result.getColumnIndex(PROFILE_COL_PRIMARY_TEL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_EMAIL)),
                        result.getString(result.getColumnIndex(PROFILE_COL_WEB)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Address)),
                        result.getString(result.getColumnIndex(PROFILE_COL_Date)),
                        result.getString(result.getColumnIndex(PROFILE_COL_IsFavorite))*/


            //-----------------Added Code--------

            String[] settingsProjection = {
                    "id",
                    PROFILE_COL_NAME,
                    PROFILE_COL_JOB_TITLE,
                    PROFILE_COL_COMPANY,
                    PROFILE_COL_PRIMARY_TEL,
                    PROFILE_COL_EMAIL,
                    PROFILE_COL_WEB,
                    PROFILE_COL_Address,
                    PROFILE_COL_Date,
                    PROFILE_COL_IsFavorite,
                    PROFILE_COL_GroupId,
                    PROFILE_COL_BlobImage,
                    PROFILE_COL_ImagePath

            };

            String whereClause = PROFILE_COL_GroupId + "=?";
            String[] whereArgs = {"'"+groupId+"'"};
/*

            Cursor resultFave = db.query(
                    PROFILE_TABLE,
                    settingsProjection,
                    whereClause,
                    whereArgs,
                    null,
                    null,
                    null
            );
*/



            Cursor resultFave = db.rawQuery("select * from  " + PROFILE_TABLE +" WHERE "+PROFILE_COL_GroupId +" = "+ groupId, null);

//            db.rawQuery("SELECT * FROM PROFILE_TABLE WHERE PROFILE_COL_GroupId = "+ someValue, null);
            //---------------------Added code end

            resultFave.moveToFirst();

            while(resultFave.isAfterLast() == false){
//
               // profile = new Profile("" + resultFave.getString(0), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_NAME)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_COMPANY)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_EMAIL)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_WEB)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_Address)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_Date)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_IsFavorite)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_GroupId)));

//                byte[] image = resultFave.getBlob(11);

                byte[] image = null;

                File imgFile = null;
                try {

                    image = resultFave.getBlob(11);
                    //String mPath = resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_ImagePath));
                    //imgFile = new File(mPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

              /*  if(imgFile.exists()){

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                    image = stream.toByteArray();


                }*/
//
                profile = new Profile("" + resultFave.getString(resultFave.getColumnIndex("id")), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_NAME)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_JOB_TITLE)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_COMPANY)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_PRIMARY_TEL)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_EMAIL)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_WEB)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_Address)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_Date)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_IsFavorite)), "" + resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_GroupId)) ,image,""+resultFave.getString(resultFave.getColumnIndex(PROFILE_COL_ImagePath)));


                favorite_profile.add(profile);
                resultFave.moveToNext();
            }

            resultFave.close();


        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        return favorite_profile;
    }


    public boolean deleteCard(int p){

        boolean  isDelete = false;

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("DELETE  FROM "+ PROFILE_TABLE + " WHERE " + "id" + " = "+p+"");
            db.close();
            isDelete = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  isDelete;
    }



    public Cursor GroupSetting() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor result = db.rawQuery("select * from  " + GROUP_SETTING_TABLE, null);
            return result;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }



    public boolean updateGroupSetting(String strgroupsettingUpdate,String strId) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            setContentValuesForGroupSetting(values, strgroupsettingUpdate);
            db.update(GROUP_SETTING_TABLE, values, "id = ?",
                    new String[]{String.valueOf(strId)});
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return false;
        }
        return true;
    }

}
