package com.bizcards.act;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bizcards.act.ui.send.SendViewModel;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import static com.bizcards.act.MainActivity.drawer;
import static com.bizcards.act.MainActivity.masterFragmentManager1;

public class ProfileViewFragment extends Fragment {

    private SendViewModel sendViewModel;


    //card_view8;

    Profile profile;
    public static final String PROFILE_ID_KEY = "profile_id_key";


    LinearLayout phone_layout,email_layout,fb_layout;
    private ProfileDao profileDao;

    private TextView textViewName;
    private TextView textViewJobTitle;
    private TextView textViewCompany;
    private TextView textViewTelephone;
    private TextView textViewEmail;

    private EditText edt_view_name, edt_view_job_title, edt_view_telephone, edt_view_email, edt_view_company;
    Context mContext;


    LinearLayout lineaEdt;
    TextView txt_edt;


    ImageView imgset;


    ImageView imgMenu_Profile_details,imgmenu;

    public static CheckBox chkfourrite;


    String ImagePathShare;


    public class MyReceiver extends BroadcastReceiver {

        public MyReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // Implement code here to be performed when
            // broadcast is detected

//            Toast.makeText(mContext,"Recevier BrodCast"+profile.getId()+""+profile.getName(),Toast.LENGTH_LONG).show();

          /*  if (chkfourrite.isChecked()) {
//                Toast.makeText(mContext,"is Checked True",Toast.LENGTH_LONG).show();
                profile.setIsFavorite("true");
                boolean isupdate = profileDao.update(profile);
                Toast.makeText(mContext, "Profile is Update " + isupdate, Toast.LENGTH_LONG).show();

            } else {
                profile.setIsFavorite("false");
                boolean isupdate = profileDao.update(profile);
                Toast.makeText(mContext, "Profile is Update " + isupdate, Toast.LENGTH_LONG).show();

            }*/


        }
    }


    //CardView  card_view8;


    boolean isEdit = false;
    MyReceiver receiver;


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mContext.unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        try {
            mContext.unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    TextView tvshareicon, txtShare,txtPhone,txtEmail,txtFB;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.activity_profile_view, container, false);

        mContext = ProfileViewFragment.this.getActivity();
        profileDao = ProfileDao.getInstance(mContext);



        phone_layout = (LinearLayout)root.findViewById(R.id.phone_layout);
        email_layout = (LinearLayout)root.findViewById(R.id.email_layout);
        fb_layout = (LinearLayout)root.findViewById(R.id.fb_layout);

        try {
            IntentFilter filter = new IntentFilter("com.example.Broadcast");
            receiver = new MyReceiver();
            mContext.registerReceiver(receiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }


        imgset = (ImageView) root.findViewById(R.id.imgset);
        tvshareicon = (TextView) root.findViewById(R.id.tvshareicon);
        txtShare = (TextView) root.findViewById(R.id.txtShare);
        txtPhone = (TextView) root.findViewById(R.id.txtPhone);
        txtEmail = (TextView) root.findViewById(R.id.txtEmail);
        txtFB = (TextView) root.findViewById(R.id.txtFB);

        textViewName = (TextView) root.findViewById(R.id.text_view_name);
        textViewJobTitle = (TextView) root.findViewById(R.id.text_view_job_title);
        textViewCompany = (TextView) root.findViewById(R.id.text_view_company);
        textViewTelephone = (TextView) root.findViewById(R.id.text_view_telephone);
        textViewEmail = (TextView) root.findViewById(R.id.text_view_email);

        edt_view_name = (EditText) root.findViewById(R.id.edt_view_name);
        edt_view_job_title = (EditText) root.findViewById(R.id.edt_view_job_title);
        edt_view_telephone = (EditText) root.findViewById(R.id.edt_view_telephone);
        edt_view_email = (EditText) root.findViewById(R.id.edt_view_email);

        edt_view_company = (EditText) root.findViewById(R.id.edt_view_company);


        txt_edt = (TextView) root.findViewById(R.id.txt_edt);
        lineaEdt = (LinearLayout) root.findViewById(R.id.lineaEdt);

        imgMenu_Profile_details = (ImageView) root.findViewById(R.id.imgMenu_Profile_details);
        imgmenu = (ImageView) root.findViewById(R.id.imgmenu);

        imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu menu = new PopupMenu(mContext, view);
                menu.getMenu().add(1, 1, 1, "Share");
                menu.getMenu().add(2, 2, 2, "Delete");
                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 1:
                                ShareImageIntent();
                                return true;
                            case 2:
                                new AlertDialog.Builder(mContext)
                                        .setTitle("Delete Card Entry")
                                        .setMessage("Are you sure you want to delete this Card entry?")

                                        // Specifying a listener allows you to take an action before dismissing the dialog.
                                        // The dialog is automatically dismissed when a dialog button is clicked.
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Continue with delete operation

                                                int mId = Integer.parseInt(profile.getId());
                                                boolean isDelte = profileDao.deleteCard(mId);

                                                if(isDelte){
                                                    Toast.makeText(mContext, "Card Deleted Successful !", Toast.LENGTH_SHORT).show();

                                                    try {
                                                        masterFragmentManager1.popBackStack();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                }else{
                                                    Toast.makeText(mContext, "Card could not Deleted Successful !", Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        })

                                        // A null listener allows the button to dismiss the dialog and take no further action.
                                        .setNegativeButton(android.R.string.no, null)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();


                                return true;

                            default:

                                return true;
                        }

                    }
                });

            }
        });


        imgMenu_Profile_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });


        chkfourrite = (CheckBox) root.findViewById(R.id.chkfourrite);

        chkfourrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "Do BrodCast", Toast.LENGTH_LONG).show();
                if (chkfourrite.isChecked()) {
//                Toast.makeText(mContext,"is Checked True",Toast.LENGTH_LONG).show();
                    profile.setIsFavorite("true");
                    boolean isupdate = profileDao.update(profile);
                    Toast.makeText(mContext, "Card is Added to Favorites ! ", Toast.LENGTH_LONG).show();

                } else {
                    profile.setIsFavorite("false");
                    boolean isupdate = profileDao.update(profile);
                    Toast.makeText(mContext, "Card is removed from Favorites !", Toast.LENGTH_LONG).show();

                }

                /*   if (chkfourrite.isChecked()) {
                 *//* Intent intent = new Intent();
                    intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                    intent.setAction("com.example.Broadcast");
                    intent.putExtra("MyData", 1000);
                    sendBroadcast(intent);*//*
                } else if (!chkfourrite.isChecked()) {

                   *//* Intent intent = new Intent();
                    intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                    intent.setAction("com.example.Broadcast");
                    intent.putExtra("MyData", 1000);
                    sendBroadcast(intent);*//*

                }*/

            }
        });


        lineaEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!isEdit) {
                    isEdit = true;
                    txt_edt.setText("Save");
                    SetEditTextView();

                } else {
                    ProcessToSaveData();
                    //SetTextView();
                }
            }
        });


        txt_edt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isEdit) {
                    isEdit = true;
                    txt_edt.setText("Save");
                    SetEditTextView();
                } else {

                    isEdit = false;
                    ProcessToSaveData();
                }
            }
        });


        tvshareicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareImageIntent();

            }
        });

        txtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareImageIntent();
            }
        });
        // rel_sel_group = (RelativeLayout)root.findViewById(R.id.rel_sel_group);

        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+Uri.encode(profile.getPrimaryContactNumber().trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });


        phone_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+Uri.encode(profile.getPrimaryContactNumber().trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });


        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + profile.getEmail()));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
//emailIntent.putExtra(Intent.EXTRA_HTML_TEXT, body); //If you are using HTML in your body text

                startActivity(Intent.createChooser(emailIntent, "Chooser Title"));
            }
        });



        email_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + profile.getEmail()));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
//emailIntent.putExtra(Intent.EXTRA_HTML_TEXT, body); //If you are using HTML in your body text

                startActivity(Intent.createChooser(emailIntent, "Chooser Title"));
            }
        });

        fb_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareImageIntent();
            }
        });

        txtFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareImageIntent();
            }
        });

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();

        SetProfileData();
    }

    private void SetProfileData() {

        isEdit = false;
        profile = loadProfile();
        if (profile == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage(R.string.profile_creator_alert_read_fail);
            builder.setPositiveButton(R.string.profile_viewer_return_to_list_button,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                            returnToList();
                            dialogInterface.dismiss();
                        }
                    });
            builder.show();

        } else {


            try {
//                byte[] image = profile.getImageBlob();
//                Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
//                imgset.setImageBitmap(bmp);

                Bitmap bmp = null;
                bmp = Utils.getBitmap(mContext, profile.getImagepath());
                imgset.setImageBitmap(bmp);

                ImagePathShare = profile.getImagepath();
            } catch (Exception e) {
                e.printStackTrace();
            }

            edt_view_name.setText(profile.getName());
            edt_view_job_title.setText(profile.getJobTitle());
            edt_view_telephone.setText(profile.getPrimaryContactNumber());
            edt_view_company.setText(profile.getCompany());
            edt_view_email.setText(profile.getEmail());

            textViewName.setText(profile.getName());
            textViewJobTitle.setText(profile.getJobTitle());
            textViewCompany.setText(profile.getCompany());
            textViewTelephone.setText(profile.getPrimaryContactNumber());
            textViewEmail.setText(profile.getEmail());

            SetTextView();

            if (profile.getIsFavorite().equalsIgnoreCase("true")) {
                chkfourrite.setChecked(true);
            } else {
                chkfourrite.setChecked(false);
            }
        }


    }

    private Profile loadProfile() {

        Profile profile = null;
        try {
            //int profileId = getIntent().getIntExtra(PROFILE_ID_KEY, -1);
            // A1 updated
            int profileId = MasterHomeFrgament.ProfileId;

            if (profileId < 0) {
                Log.d(ProfileViewActivity.class.getName(),
                        "Profile ID is not passed on from the previous activity");
            } else {
                profile = profileDao.load(profileId);
            }
        } catch (Exception e) {
            Log.e(ProfileViewActivity.class.getName(), "Failed to load profile!");
            Log.e(ProfileViewActivity.class.getName(), Log.getStackTraceString(e));
        }

        return profile;
    }


    private void SetTextView() {

        edt_view_name.setVisibility(View.GONE);
        edt_view_job_title.setVisibility(View.GONE);
        edt_view_telephone.setVisibility(View.GONE);
        edt_view_email.setVisibility(View.GONE);
        edt_view_company.setVisibility(View.GONE);

        textViewName.setVisibility(View.VISIBLE);
        textViewJobTitle.setVisibility(View.VISIBLE);
        textViewCompany.setVisibility(View.VISIBLE);
        textViewTelephone.setVisibility(View.VISIBLE);
        textViewEmail.setVisibility(View.VISIBLE);


    }


    private void SetEditTextView() {

        edt_view_name.setVisibility(View.VISIBLE);
        edt_view_job_title.setVisibility(View.VISIBLE);
        edt_view_telephone.setVisibility(View.VISIBLE);
        edt_view_email.setVisibility(View.VISIBLE);
        edt_view_company.setVisibility(View.VISIBLE);

        textViewName.setVisibility(View.GONE);
        textViewJobTitle.setVisibility(View.GONE);
        textViewCompany.setVisibility(View.GONE);
        textViewTelephone.setVisibility(View.GONE);
        textViewEmail.setVisibility(View.GONE);

    }


    private void ProcessToSaveData() {

        isEdit = false;
        txt_edt.setText("Edit");

        String strname = edt_view_name.getText().toString();
        String strtitle = edt_view_job_title.getText().toString();
        String strtele = edt_view_telephone.getText().toString();
        String stremail = edt_view_email.getText().toString();
        String strcompany = edt_view_company.getText().toString();

        profile.setName(strname);
        profile.setJobTitle(strtitle);
        profile.setPrimaryContactNumber(strtele);
        profile.setEmail(stremail);
        profile.setCompany(strcompany);


        boolean isupdate = profileDao.update(profile);

        Toast.makeText(mContext, "Edit SuccessFull !", Toast.LENGTH_LONG).show();

//        loadProfile();

        SetProfileData();
    }




    private void ShareImageIntent(){

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+ImagePathShare));
        startActivity(Intent.createChooser(share, "Share Image"));
    }




}
